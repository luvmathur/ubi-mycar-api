from core import WebService, request, abort, response
from core import utils, hooks, has_resource, get_resource
from stslib.database import models, errors
from hashlib import sha1
from dateutils import relativedelta
from datetime import datetime, timedelta, date
from rss2producer import RSS2Feed
from quik import Template
import requests
import re
import time
import json
import random

from report_impl import driver_score

service = WebService('report', 'app.config')

LIMIT_IDLE = [
    (0, 40, "A+"), (40, 80, "A"), (80, 120, "A-"),
    (120, 180, "B+"), (180, 240, "B"), (240, 300, "B-"),
    (300, 400, "C+"), (400, 500, "C"), (500, 600, "C-"),
    (600, 760, "D+"), (760, 920, "D"), (920, 1080, "D-"),
]

LIMIT_PERCENT = [
    (1, .966667, "A+"), (.966667, .933333, "A"), (.933333, .9, "A-"),
    (.9, .866667, "B+"), (.866667, .833333, "B"), (.833333, .8, "B-"),
    (.8, .766667, "C+"), (.766667, .733333, "C"), (.733333, .7, "C-"),
    (.7, .666667, "D+"), (.666667, .633333, "D"), (.633333, .6, "D-"),
]


def getIdleGrade(v, days=1):
    for min, max, grade in LIMIT_IDLE:
        if min <= (v / days) <= max:
            return grade;
    return "F"


def getGradePercent(v):
    for max, min, grade in LIMIT_PERCENT:
        if v == grade:
            return max;
    return 0.0


def getPrecentGrade(v, rev=False):
    if rev: v = 1 - v;
    for max, min, grade in LIMIT_PERCENT:
        if max >= v >= min:
            return grade;
    return "F"


STATUS_MOTION_START = 0xF111  # 61713
STATUS_MOTION_IN_MOTION = 0xF112  # 61714
STATUS_MOTION_STOP = 0xF113  # 61715
# STATUS_MOTION_DORMANT       = 0xF114   # 61716
STATUS_MOTION_IDLE = 0xF116  # 61718
STATUS_MOTION_IDLE_END = 0xF117  # 61719
STATUS_MOTION_EXCESS_IDLE = 0xF118  # 61720
STATUS_MOTION_EXCESS_SPEED = 0xF11A  # 61722
STATUS_MOTION_OVER_SPEED_1 = 0xF11A  # 61722
STATUS_MOTION_OVER_SPEED_2 = 0xF11B  # 61723
STATUS_MOTION_MOVING = 0xF11C  # 61724
STATUS_MOTION_STOP_PENDING = 0xF11D  # 61725
STATUS_MOTION_CHANGE = 0xF11E  # 61726
STATUS_MOTION_HEADING = 0xF11F  # 61727
STATUS_MOTION_ACCELEROMETER = 0xF120  # 61728
STATUS_MOTION_ACCELERATION = 0xF123  # 61731
STATUS_EXCESS_ACCELERATION = 0xF960  # 63840
STATUS_MOTION_DECELERATION = 0xF126  # 61734
STATUS_EXCESS_DECELERATION = 0xF930  # 63792
STATUS_EXCESS_CORNERING = 0xF937  # 63799
STATUS_EXCESS_CORNERING_2 = 0xF938  # 63800
STATUS_EXCESS_CORNERING_3 = 0xF939  # 63801

CUSTOM_EXCESS_ACCELERATION_1 = 0xA410  # 42000
CUSTOM_EXCESS_ACCELERATION_2 = 0xA411  # 42001
CUSTOM_EXCESS_ACCELERATION_3 = 0xA412  # 42002
CUSTOM_EXCESS_ACCELERATION_4 = 0xA413  # 42003
CUSTOM_EXCESS_DECELERATION_1 = 0xA41A  # 42010
CUSTOM_EXCESS_DECELERATION_2 = 0xA41B  # 42011
CUSTOM_EXCESS_DECELERATION_3 = 0xA41C  # 42012
CUSTOM_EXCESS_DECELERATION_4 = 0xA41D  # 42013
CUSTOM_EXCESS_L_CORNERING_1 = 0xA424  # 42020
CUSTOM_EXCESS_L_CORNERING_2 = 0xA425  # 42021
CUSTOM_EXCESS_L_CORNERING_3 = 0xA426  # 42022
CUSTOM_EXCESS_R_CORNERING_1 = 0xA427  # 42023
CUSTOM_EXCESS_R_CORNERING_2 = 0xA428  # 42024
CUSTOM_EXCESS_R_CORNERING_3 = 0xA429  # 42025

EVNTS_STEERING = [
    # STATUS_EXCESS_CORNERING,
    # STATUS_EXCESS_CORNERING_2
    # STATUS_EXCESS_CORNERING_3,
    CUSTOM_EXCESS_L_CORNERING_1,
    # CUSTOM_EXCESS_L_CORNERING_2,
    # CUSTOM_EXCESS_L_CORNERING_3,
    CUSTOM_EXCESS_R_CORNERING_1,
    # CUSTOM_EXCESS_R_CORNERING_2,
    # CUSTOM_EXCESS_R_CORNERING_3,
]

EVNTS_MOTION = [
    STATUS_MOTION_START,
    STATUS_MOTION_IN_MOTION,
    STATUS_MOTION_STOP,
    STATUS_MOTION_EXCESS_SPEED,
    STATUS_MOTION_OVER_SPEED_1,
    STATUS_MOTION_OVER_SPEED_2,
    STATUS_MOTION_MOVING,
    STATUS_MOTION_STOP_PENDING,
    STATUS_MOTION_CHANGE,
    STATUS_MOTION_HEADING,
    STATUS_MOTION_ACCELEROMETER,
    STATUS_MOTION_ACCELERATION,
    STATUS_MOTION_DECELERATION,
    STATUS_EXCESS_CORNERING,
    STATUS_EXCESS_CORNERING_2,
    STATUS_EXCESS_CORNERING_3,
    STATUS_EXCESS_DECELERATION,
    STATUS_EXCESS_ACCELERATION,
    CUSTOM_EXCESS_ACCELERATION_1,
    CUSTOM_EXCESS_ACCELERATION_2,
    CUSTOM_EXCESS_ACCELERATION_3,
    CUSTOM_EXCESS_ACCELERATION_4,
    CUSTOM_EXCESS_DECELERATION_1,
    CUSTOM_EXCESS_DECELERATION_2,
    CUSTOM_EXCESS_DECELERATION_3,
    CUSTOM_EXCESS_DECELERATION_4,
    CUSTOM_EXCESS_L_CORNERING_1,
    CUSTOM_EXCESS_L_CORNERING_2,
    CUSTOM_EXCESS_L_CORNERING_3,
    CUSTOM_EXCESS_R_CORNERING_1,
    CUSTOM_EXCESS_R_CORNERING_2,
    CUSTOM_EXCESS_R_CORNERING_3,
]

EVNTS_IDLE = [
    STATUS_MOTION_IDLE,
    STATUS_MOTION_EXCESS_IDLE,
]

EVNTS_IDLE_END = [
    STATUS_MOTION_IDLE_END,
    STATUS_MOTION_STOP
]

EVNTS_OVERSPEED = [
    STATUS_MOTION_EXCESS_SPEED,
    STATUS_MOTION_OVER_SPEED_1,
    STATUS_MOTION_OVER_SPEED_2,
]

EVNTS_ACCELERATION = [
    # STATUS_MOTION_ACCELERATION,
    # STATUS_EXCESS_ACCELERATION,
    CUSTOM_EXCESS_ACCELERATION_1,
    # CUSTOM_EXCESS_ACCELERATION_2,
    # CUSTOM_EXCESS_ACCELERATION_3,
    # CUSTOM_EXCESS_ACCELERATION_4,
]

EVNTS_DECELERATION = [
    # STATUS_MOTION_DECELERATION,
    # STATUS_EXCESS_DECELERATION,
    CUSTOM_EXCESS_DECELERATION_1,
    # CUSTOM_EXCESS_DECELERATION_2,
    # CUSTOM_EXCESS_DECELERATION_3,
    # CUSTOM_EXCESS_DECELERATION_4,
]


def create_trip_from_agg_trip(r, fuel_capacity):
    """
    Takes an aggregated trip record from the TripDetail/TripDetailEvents tables
    and converts it for direct use by the trip end-point.

    Handles data conversion from format stored in database to the format
    expected by the internal functions in the trip endpoint.
    """
    _trip = {
        '__duration': r.tripStop - r.tripStart,
        'distance': r.totalDistanceKM,
        'duration': str(r.tripStop - r.tripStart),
        'end_address': r.endAddress.decode('latin-1').encode('utf-8'),
        'end_datetime': r.details[-1].timestamp,
        'end_latitude': float(r.endLatitude),
        'end_longitude': float(r.endLongitude),
        'fuel_usage': float(r.totalFuelL / fuel_capacity),
        'data_source': 'aggregate',
    }

    _details = [{
                    'address': d.address.decode('latin-1').encode('utf8'),
                    'distance': d.distance,
                    'distanceTotal': d.distanceTotal,
                    'heading': d.heading,
                    'latitude': float(d.latitude),
                    'longitude': float(d.longitude),
                    'speed': d.speed,
                    'statusCode': d.statusCode,
                    'timestamp': d.timestamp
                } for d in r.details]

    return _trip, _details


@service.priv_route('/v<version>/report/trip', method=['GET', 'OPTIONS'])
@service.priv_route('/v<version>/report/trip/<deviceid>', method=['GET', 'OPTIONS'])
@hooks.checksum(sha1)
def trip(account, user, groups, db, version, deviceid=None):
    """Trips Report

    GET: /reports/trip
    GET: /reports/trip/<deviceid>

    This route offers reporting for trip details.
    Data is returned as a list of days. each day has a summary and a
    list of trips by start date. each trip has a summary and a list of
    waypoints on the trip.
    """
    if request.method == "OPTIONS": return

    service.logger.debug("{0} GET: /trip, /trip/<deviceid>".format(version))
    service.logger.debug("DEVICEID: {0}".format(deviceid))

    convert = utils.UnitConverter(account)

    if deviceid is None:
        deviceid = user.preferredDeviceID

    ## Rolling 7 Days ##
    limit_dts = int(time.time() - (7 * 24 * 60 * 60))

    ## get records
    _vehicle = db.query(models.Device) \
        .filter(models.Device.accountID == account.accountID) \
        .filter(models.Device.deviceID == deviceid) \
        .first()

    ## Query events for this device but limit to fair or better gps dilution quality ##
    _records = db.query(models.TripActivityEvents) \
        .filter(models.TripActivityEvents.accountID == account.accountID) \
        .filter(models.TripActivityEvents.deviceID == deviceid) \
        .filter(models.TripActivityEvents.gps_timestamp > limit_dts) \
        .order_by('gps_timestamp')

    if not _vehicle or not _records:
        abort(404, "Not Found")

    ret = {}
    _inTrip = False
    _timestamp = 0
    _loc = None
    _fuel = None
    ## New Fuel Calc
    # _fuel = []
    # _fuel_t = []
    ## New Fuel Calc

    _tripData = []
    _trip = {
        'start_address': '',
        'start_datetime': 0,
        'end_datetime': 0,
        'distance': 0,
    }

    def add_trip_to_catalog(trip, catalog):
        _start_datetime = trip['detail'][0]['timestamp']

        _distance_raw = float(trip['__distance_raw'])
        _fuel_usage_raw = float(trip['__fuel_usage_raw'])
        _group_index = utils.ts2intday(_start_datetime, account.timeZone)
        _group_label = utils.ts2strdate_short2(_start_datetime, account.timeZone)
        _index = _start_datetime

        record = catalog.get(_group_index, {'trips': {'0': {'label': 'Total'}}})
        record_summary = record['trips']['0']

        record_summary['__distance'] = _distance_raw + record_summary.get('__distance', 0)
        record_summary['__fuel_usage'] = _fuel_usage_raw + record_summary.get('__fuel_usage', 0)
        record_summary['__duration'] = trip['__duration'] + record_summary.get('__duration', 0)
        record_summary['distance'] = convert.distance2local(record_summary['__distance'])
        record_summary['fuel_usage'] = convert.volume2local(record_summary['__fuel_usage'])
        record_summary['duration'] = str(timedelta(seconds=record_summary['__duration']))

        if record_summary['__fuel_usage'] > 0:
            eco = record_summary['__distance'] / record_summary['__fuel_usage']
            record_summary['economyScore'] = float(eco / float(_vehicle.fuelEconomy))
            record_summary['economyValue'] = convert.economy2local(eco)
        else:
            record_summary['economyScore'] = 1
            record_summary['economyValue'] = 0

        record['trips'][_index] = trip
        record['trips']['0'] = record_summary
        record['label'] = _group_label

        catalog[_group_index] = record

    def finalize_trip(trip, detail, catalog):
        if len(detail) <= 1:
            return
        if trip['distance'] < 0.25:
            return

        verbose = _trip.get('_VERBOSE', False)
        _start_datetime = detail[0]['timestamp']
        _start_lat = detail[0]['latitude']
        _start_lon = detail[0]['longitude']
        _fuel_usage_raw = float(trip['fuel_usage']) * float(_vehicle.fuelCapacity)
        _distance_raw = float(trip['distance'])
        _label = utils.ts2strtime_short2(_start_datetime, account.timeZone)

        if verbose: print("Raw Fuel Usage:", _fuel_usage_raw)

        if _fuel_usage_raw > 0:
            eco = _distance_raw / _fuel_usage_raw
            trip['economyScore'] = float(eco / float(_vehicle.fuelEconomy))
            trip['economyValue'] = convert.economy2local(eco)
        else:
            trip['economyScore'] = 1
            trip['economyValue'] = 0

        trip['__distance_raw'] = _distance_raw
        trip['distance'] = convert.distance2local(_distance_raw)
        trip['__fuel_usage_raw'] = _fuel_usage_raw
        trip['fuel_usage'] = convert.volume2local(_fuel_usage_raw)
        trip['__duration'] = trip['end_datetime'] - _start_datetime
        trip['duration'] = str(timedelta(seconds=trip['end_datetime'] - _start_datetime))
        trip['end_datetime'] = utils.timestamp2tztime(trip['end_datetime'], account.timeZone).strftime('%a %b %d,%I:%M:%S %p')
        trip['start_address'] = detail[0]['address']
        trip['start_datetime'] = utils.timestamp2tztime(_start_datetime, account.timeZone).strftime('%a %b %d,%I:%M:%S %p')
        trip['start_latitude'] = _start_lat
        trip['start_longitude'] = _start_lon
        trip['detail'] = detail
        trip['label'] = _label

        add_trip_to_catalog(trip, catalog)

    _agg_trips = db.query(models.TripDetail) \
        .filter(models.TripDetail.deviceUUID == _vehicle.deviceHistoryID) \
        .filter(models.TripDetail.tripStart >= datetime.utcfromtimestamp(limit_dts)) \
        .order_by('tripStart')
    for record in _agg_trips:
        t, d = create_trip_from_agg_trip(record, _vehicle.fuelCapacity)
        finalize_trip(t, d, ret)

    _pfuel = None
    _pfuelOffset = 0
    for row in _records:
        _ntimestamp = row.gps_timestamp
        _delta = _ntimestamp - _timestamp
        if _delta > (60 * 7): _inTrip = False

        _nloc = (row.latitude, row.longitude)
        if _loc:
            _distance = utils.getRadialDistance(_loc, _nloc)
        else:
            _distance = 0

        if _fuel is None: _pfuel = _fuel = row.fuelLevel

        if _pfuel:
            if float(row.fuelLevel - _pfuel) > .10:
                _pfuelOffset += _fuel - _pfuel
                _fuel = row.fuelLevel
                # print(_pfuelOffset, row.fuelLevel)

        _pfuel = row.fuelLevel
        if not _inTrip:
            ## Trip End ##


            a = _fuel - row.fuelLevel
            _trip['fuel_usage'] = a if a > 0 else 0
            _trip['fuel_usage'] += _pfuelOffset

            finalize_trip(_trip, _tripData, ret)

            _inTrip = True
            _tripData = []
            _total_distance = 0
            _distance = 0
            _pfuel = _fuel = row.fuelLevel
            _pfuelOffset = 0
            # _loc = None

            ## Trip Start ##
            _trip = {
                'distance': 0,
                'fuel_usage': 0,
            }

        _trip['end_address'] = row.address
        _trip['end_datetime'] = row.gps_timestamp
        _trip['end_latitude'] = float(row.latitude)
        _trip['end_longitude'] = float(row.longitude)
        _trip['distance'] += _distance

        _timestamp = _ntimestamp
        _loc = _nloc

        _tripData.append({
            'statusCode': row.statusCode,
            'latitude': float(row.latitude),
            'longitude': float(row.longitude),
            'distance': convert.distance2local(_distance),
            'distanceTotal': convert.distance2local(_trip['distance']),
            'timestamp': _ntimestamp,
            'heading': float(row.heading),
            'speed': convert.speed2local(row.speedKPH),
            'address': row.address,
        })

    ## Add last leg
    if _inTrip:
        a = _fuel - row.fuelLevel
        _trip['fuel_usage'] = a if a > 0 else 0
        finalize_trip(_trip, _tripData, ret)

    return {'trips': ret, 'deviceID': deviceid}


@service.priv_route('/v<version>/report/economy', method=['GET', 'OPTIONS'])
@service.priv_route('/v<version>/report/economy/<deviceid>', method=['GET', 'OPTIONS'])
@hooks.checksum(sha1)
def economy(account, user, groups, db, version, deviceid=None):
    """Eco Grade

    GET: /reports/economy
    GET: /reports/economy/<deviceid>

    This route will provide eco grade detail for a specific device.
    """
    if request.method == "OPTIONS": return

    service.logger.debug("{0} GET: /report/economy, /report/economy/<deviceid>".format(version))
    service.logger.debug("DEVICEID: {0}".format(deviceid))

    convert = utils.UnitConverter(account)

    if deviceid is None:
        deviceid = user.preferredDeviceID

    # figure out our period starts
    today = datetime.utcnow().date()
    four_months_earlier = today - relativedelta(months=4)
    start_month = date(four_months_earlier.year, four_months_earlier.month, 1)
    start_week = today - relativedelta(weeks=4, days=int(today.strftime('%w')))
    start_day = today - relativedelta(days=4)

    ts_start_month = int(start_month.strftime("%s"))
    ts_start_week = int(start_week.strftime("%s"))
    ts_start_day = int(start_day.strftime("%s"))

    current_month = int((today - relativedelta(days=(today.day - 1))).strftime("%s"))
    current_week = int((today - relativedelta(days=int(today.strftime('%w')))).strftime("%s"))
    current_day = int(today.strftime("%s"))

    ## Pull Records
    _vehicle = db.query(models.Device) \
        .filter(models.Device.accountID == account.accountID) \
        .filter(models.Device.deviceID == deviceid) \
        .first()

    _records = db.query(models.EconomyActivityEvents) \
        .filter(models.EconomyActivityEvents.accountID == account.accountID) \
        .filter(models.EconomyActivityEvents.deviceID == deviceid) \
        .filter(models.EconomyActivityEvents.gps_timestamp > int(ts_start_month)) \
        .order_by('gps_timestamp')

    ret = {
        'monthly': {},
        'weekly': {},
        'daily': {},
    }

    def compute_metrics(catalog):
        ## CO2 Calculation Constants
        __co2_octaneP = float(0.8)
        __co2_fuelWeight_lbs = float(1.651041882)
        __co2_atomRatio = float(352.0 / 114.0)

        economy = 0
        goal = (float(_vehicle.fuelEconomy) * 0.8) if _vehicle.fuelEconomy > 0 else 12.75  ## blind goal of 30 MPG
        epa_goal = (float(_vehicle.fuelEconomy)) if _vehicle.fuelEconomy > 0 else 14.88  ## blind goal of 35 MPG

        if catalog['__fuelUsed'] > 0:
            # Economy
            economy = catalog['__distance'] / catalog['__fuelUsed']
            catalog['economy'] = convert.economy2local(economy)

            ## Carbon
            co2 = float(catalog['__fuelUsed'] * __co2_octaneP * __co2_fuelWeight_lbs * __co2_atomRatio)
            catalog['Co2Value'] = co2

            co2_goal = float(catalog['__distance'] / epa_goal) * __co2_octaneP * __co2_fuelWeight_lbs * __co2_atomRatio
            catalog['Co2Goal'] = co2_goal

            if co2 < co2_goal:
                catalog['Co2Score'] = 1
                catalog['Co2Grade'] = "A+"
            elif (co2_goal > 0):
                catalog['Co2Score'] = 1 - ((co2 - co2_goal) / co2_goal)
                if catalog['Co2Score'] < 0: catalog['Co2Score'] = 0
                catalog['Co2Grade'] = getPrecentGrade(catalog['Co2Score'])
            else:
                catalog['Co2Score'] = 1
                catalog['Co2Grade'] = "NA"
                catalog['Co2Value'] = "0"

            ## Fuel/Dist
            catalog['FuelDistScore'] = catalog['__distance'] / (catalog['__fuelUsed'] * epa_goal)
            if catalog['FuelDistScore'] > 1: catalog['FuelDistScore'] = 1
            catalog['FuelDistGrade'] = getPrecentGrade(catalog['FuelDistScore'])
        else:
            # Economy
            catalog['economy'] = 0

            # Carbon
            catalog['Co2Score'] = 1
            catalog['Co2Grade'] = "NA"
            catalog['Co2Value'] = "0"

            # Fuel/Dist
            catalog['FuelDistScore'] = 1
            catalog['FuelDistGrade'] = "NA"

        if economy > 0:
            ## EconomyScore
            catalog['EconomyScore'] = economy / goal
            if catalog['EconomyScore'] > 1: catalog['EconomyScore'] = 1
            catalog['EconomyGrade'] = getPrecentGrade(catalog['EconomyScore'])
        else:
            catalog['EconomyScore'] = 1
            catalog['EconomyGrade'] = "NA"

        if catalog['__engine_time'] > 0:
            # convert engine time seconds to days, so that we don't have to modify
            # existing min/max boundaries. TODO refactor away if we modify bounds
            # to be based on seconds instead of days.
            days = catalog['__engine_time'] / 86400.0
            catalog['IdleGrade'] = getIdleGrade(catalog['__idle_time'], days)
            catalog['IdleScore'] = getGradePercent(catalog['IdleGrade'])
        else:
            catalog['IdleGrade'] = "NA"
            catalog['IdleScore'] = 1

        if catalog['__engine_time'] > 0:
            catalog['SmoothStartsScore'] = driver_score(catalog['__engine_time'], catalog['__accel'])
            catalog['SmoothStopsScore'] = driver_score(catalog['__engine_time'], catalog['__breaking'])
            catalog['SteeringScore'] = driver_score(catalog['__engine_time'], catalog['__steering'])
            catalog['AccelGrade'] = getPrecentGrade(catalog['SmoothStartsScore'])
            catalog['BreakGrade'] = getPrecentGrade(catalog['SmoothStopsScore'])
            catalog['SteeringGrade'] = getPrecentGrade(catalog['SteeringScore'])
        else:
            catalog['SmoothStartsScore'] = 1
            catalog['SmoothStopsScore'] = 1
            catalog['SteeringScore'] = 1
            catalog['AccelGrade'] = "NA"
            catalog['BreakGrade'] = "NA"
            catalog['SteeringGrade'] = "NA"

        catalog['OverallScore'] = (
            (catalog['SmoothStartsScore'] + catalog['SmoothStopsScore'] + catalog['SteeringScore']) / 3
        )

        if (catalog['OverallScore'] > 1):
            catalog['OverallScore'] = 1
            catalog['OverallGrade'] = 'A+'
        else:
            catalog['OverallGrade'] = getPrecentGrade(catalog['OverallScore'])

        return catalog

    def init_catalog(label):
        return {
            '__distance': 0,
            '__fuelUsed': 0,
            '__economy': 0,
            '__economy_count': 0,
            '__accel': 0,
            '__breaking': 0,
            '__motion': 0,
            '__idle_time': 0,
            '__engine_time': 0,
            '__total_count': 0,
            '__overspeed': 0,
            '__steering': 0,
            'label': label,
        }

    def fill_metrics(catalog, **kwargs):
        """
        Keyword Arguments:
            label         :  The label for this catalog
            distance      :  The total distance traveled between this and the previous event
            economy       :  The fuel usage ratio as provided by the MAS
            economy_count :  The fuel usage ratio as provided by the MAS
            accel         :  The count of events exceeding the acceleration Threshold
            breaking      :  The count of events exceeding the deceleration Threshold
            motion        :  The count of events representing the car in motion
            overspeed     :  The count of events representing the car in motion over the speed threshold
            steering      :  The count of events representing the car swerving.
            idle_time     :  The duration between this and the last event if applicable
            engine_time   :  The duration between this and the last event if applicable
        """
        # service.logger.debug(kwargs)
        if catalog is None:
            catalog = init_catalog(kwargs['label'])

        ## Measures
        catalog['__distance'] += kwargs['distance']
        catalog['__fuelUsed'] += kwargs['fuelUsed']
        catalog['__accel'] += kwargs['accel']
        catalog['__breaking'] += kwargs['breaking']
        catalog['__motion'] += kwargs['motion']
        catalog['__idle_time'] += kwargs['idle_time']
        catalog['__engine_time'] += kwargs['engine_time']
        catalog['__overspeed'] += kwargs['overspeed']
        catalog['__steering'] += kwargs['steering']
        catalog['__total_count'] += 1

        catalog['distance'] = convert.distance2local(catalog['__distance'])

        ## Metrics
        return compute_metrics(catalog)

    def process_aggregate_record(catalog, record, label):
        economy = float(record.totalDistanceKM / record.totalFuelL) if record.totalFuelL > 0.0 else 0.0
        fill_metrics(catalog,
                     label=label,
                     distance=float(record.totalDistanceKM),
                     economy=economy,
                     economy_count=0,
                     accel=record.evntAccelerationCount,
                     breaking=record.evntDecelerationCount,
                     motion=record.evntMotionCount,
                     overspeed=record.evntOverspeedCount,
                     steering=record.evntCorneringCount,
                     fuelUsed=float(record.totalFuelL),
                     idle_time=record.idleSeconds,
                     engine_time=record.engineSeconds,
                     )
        return catalog

    # initialize all of the catalogs that we're going to fill
    for period_index in range(0, 5):
        ts_month = int((start_month + relativedelta(months=period_index)).strftime("%s"))
        month_label = utils.ts2strmonth(ts_month, account.timeZone)
        month_index = utils.ts2intmonth(ts_month, account.timeZone)
        ret["monthly"][month_index] = init_catalog(month_label)

        ts_week = int((start_week + relativedelta(weeks=period_index)).strftime("%s"))
        week_label = utils.ts2strweek(ts_week, account.timeZone)
        week_index = utils.ts2intweek(ts_week, account.timeZone)
        ret["weekly"][week_index] = init_catalog(week_label)

        ts_day = int((start_day + relativedelta(days=period_index)).strftime("%s"))
        day_label = utils.ts2strday(ts_day, account.timeZone)
        day_index = utils.ts2intday(ts_day, account.timeZone)
        ret["daily"][day_index] = init_catalog(day_label)

    # also pre-initialize the "overall" summary catalogs
    ret['monthly']['overall'] = init_catalog("Last 5 Months")
    ret['weekly']['overall'] = init_catalog("Last 5 Weeks")
    ret['daily']['overall'] = init_catalog("Last 5 Days")

    _agg_months = db.query(models.AggregateEvent_Monthly) \
        .filter(models.AggregateEvent_Monthly.deviceUUID == _vehicle.deviceHistoryID) \
        .filter(models.AggregateEvent_Monthly.frameStart >= datetime.utcfromtimestamp(ts_start_month)) \
        .order_by(models.AggregateEvent_Monthly.frameStart)
    for month in _agg_months:
        _origin_ts = int(month.frameStart.strftime("%s"))
        _label = utils.ts2strmonth(_origin_ts, account.timeZone)
        _month_index = utils.ts2intmonth(_origin_ts, account.timeZone)
        _catalog = ret['monthly'].get(_month_index)
        ret["monthly"][_month_index] = process_aggregate_record(_catalog, month, _label)

        _month_summary_catalog = ret['monthly'].get('overall')
        ret["monthly"]["overall"] = process_aggregate_record(
            _month_summary_catalog, month, _label)

    _agg_weeks = db.query(models.AggregateEvent_Weekly) \
        .filter(models.AggregateEvent_Weekly.deviceUUID == _vehicle.deviceHistoryID) \
        .filter(models.AggregateEvent_Weekly.frameStart >= datetime.utcfromtimestamp(ts_start_week)) \
        .order_by(models.AggregateEvent_Weekly.frameStart)
    for week in _agg_weeks:
        _origin_ts = int(week.frameStart.strftime("%s"))
        _label = utils.ts2strweek(_origin_ts, account.timeZone)
        _week_index = utils.ts2intweek(_origin_ts, account.timeZone)
        _catalog = ret['weekly'].get(_week_index)
        ret["weekly"][_week_index] = process_aggregate_record(_catalog, week, _label)

        _week_summary_catalog = ret['weekly'].get('overall')
        ret["weekly"]["overall"] = process_aggregate_record(
            _week_summary_catalog, week, _label)

    _agg_days = db.query(models.AggregateEvent_Daily) \
        .filter(models.AggregateEvent_Daily.deviceUUID == _vehicle.deviceHistoryID) \
        .filter(models.AggregateEvent_Daily.frameStart >= datetime.utcfromtimestamp(ts_start_day)) \
        .order_by(models.AggregateEvent_Daily.frameStart)
    for day in _agg_days:
        _origin_ts = int(day.frameStart.strftime("%s"))
        _label = utils.ts2strday(_origin_ts, account.timeZone)
        _day_index = utils.ts2intday(_origin_ts, account.timeZone)
        _catalog = ret['daily'].get(_day_index)
        ret["daily"][_day_index] = process_aggregate_record(_catalog, day, _label)

        _day_summary_catalog = ret['daily'].get('overall')
        ret["daily"]["overall"] = process_aggregate_record(
            _day_summary_catalog, day, _label)

    lastRow = None
    idling = False
    for row in _records:

        timestamp = row.gps_timestamp

        ## Setup measures
        f_breaking = None
        f_distance = 0
        f_accel = 0
        f_break = 0
        f_motion = 0
        f_idle_time = 0
        f_overspeed = 0
        f_economy = float(row.fuelEconomy or 0)
        f_economy_count = 0
        f_steering = 0
        f_fuelUsed = 0
        f_engineTime = 0

        # if f_economy == float('inf'):
        # service.logger.debug(row.timestamp);

        ## Calculate Measures
        if lastRow:
            f_distance = float(utils.getRadialDistance(
                (lastRow.latitude, lastRow.longitude),
                (row.latitude, row.longitude)
            ))

            f_fuelUsed = float(lastRow.fuelLevel - row.fuelLevel)

            ## Ignore fillup by using negative use outside of a threshold
            if f_fuelUsed < -0.20 or f_fuelUsed > 0.20 or not _vehicle.fuelCapacity > 0:
                f_fuelUsed = 0

            ## Convert from percent to Litres
            __x = f_fuelUsed
            f_fuelUsed = f_fuelUsed * float(_vehicle.fuelCapacity)

            # if the events are within ENGINE_TIME_GAP seconds of each other
            # count the difference as "engine time"
            ENGINE_TIME_GAP = 7 * 60
            event_ts_diff = timestamp - lastRow.gps_timestamp
            f_engineTime = max(0, event_ts_diff) if event_ts_diff <= ENGINE_TIME_GAP else 0.0

            # if we're idling, then add the time between events as idle time.
            # it's important that this happen before the code below, since that code
            # toggles the idle state.
            if idling:
                f_idle_time = event_ts_diff

        if row.statusCode in EVNTS_ACCELERATION:
            f_accel = 1

        if row.statusCode in EVNTS_DECELERATION:
            f_break = 1

        if row.statusCode in EVNTS_IDLE:
            idling = True

        if row.statusCode in EVNTS_IDLE_END:
            idling = False

        if row.statusCode in EVNTS_MOTION:
            f_motion = 1

        if row.statusCode in EVNTS_OVERSPEED:
            f_overspeed = 1

        if row.statusCode in EVNTS_STEERING:
            f_steering = 1

        if f_economy > 0:
            f_economy_count = 1

        ## Monthly Metric Fill
        _month_index = utils.ts2intmonth(timestamp, account.timeZone)
        _month_catalog = ret['monthly'].get(_month_index)

        ret['monthly'][_month_index] = fill_metrics(_month_catalog,
                                                    label=utils.ts2strmonth(timestamp, account.timeZone),
                                                    distance=f_distance,
                                                    economy=f_economy,
                                                    economy_count=f_economy_count,
                                                    accel=f_accel,
                                                    breaking=f_break,
                                                    motion=f_motion,
                                                    overspeed=f_overspeed,
                                                    steering=f_steering,
                                                    fuelUsed=f_fuelUsed,
                                                    idle_time=f_idle_time,
                                                    engine_time=f_engineTime,
                                                    )

        _month_summary_catalog = ret['monthly'].get('overall')
        ret['monthly']['overall'] = fill_metrics(_month_summary_catalog,
                                                 label=utils.ts2strmonth(timestamp, account.timeZone),
                                                 distance=f_distance,
                                                 economy=f_economy,
                                                 economy_count=f_economy_count,
                                                 accel=f_accel,
                                                 breaking=f_break,
                                                 motion=f_motion,
                                                 overspeed=f_overspeed,
                                                 steering=f_steering,
                                                 fuelUsed=f_fuelUsed,
                                                 idle_time=f_idle_time,
                                                 engine_time=f_engineTime,
                                                 )

        if timestamp >= ts_start_week:
            _week_index = utils.ts2intweek(timestamp, account.timeZone)
            _week_catalog = ret['weekly'].get(_week_index)

            ret['weekly'][_week_index] = fill_metrics(_week_catalog,
                                                      label=utils.ts2strweek(timestamp, account.timeZone),
                                                      distance=f_distance,
                                                      economy=f_economy,
                                                      economy_count=f_economy_count,
                                                      accel=f_accel,
                                                      breaking=f_break,
                                                      motion=f_motion,
                                                      overspeed=f_overspeed,
                                                      steering=f_steering,
                                                      fuelUsed=f_fuelUsed,
                                                      idle_time=f_idle_time,
                                                      engine_time=f_engineTime,
                                                      )

            _week_summary_catalog = ret['weekly'].get('overall')
            ret['weekly']['overall'] = fill_metrics(_week_summary_catalog,
                                                    label=utils.ts2strweek(timestamp, account.timeZone),
                                                    distance=f_distance,
                                                    economy=f_economy,
                                                    economy_count=f_economy_count,
                                                    accel=f_accel,
                                                    breaking=f_break,
                                                    motion=f_motion,
                                                    overspeed=f_overspeed,
                                                    steering=f_steering,
                                                    fuelUsed=f_fuelUsed,
                                                    idle_time=f_idle_time,
                                                    engine_time=f_engineTime,
                                                    )

        if timestamp >= ts_start_day:
            _day_index = utils.ts2intday(timestamp, account.timeZone)
            _day_catalog = ret['daily'].get(_day_index)

            ret['daily'][_day_index] = fill_metrics(_day_catalog,
                                                    label=utils.ts2strday(timestamp, account.timeZone),
                                                    distance=f_distance,
                                                    economy=f_economy,
                                                    economy_count=f_economy_count,
                                                    accel=f_accel,
                                                    breaking=f_break,
                                                    motion=f_motion,
                                                    overspeed=f_overspeed,
                                                    steering=f_steering,
                                                    fuelUsed=f_fuelUsed,
                                                    idle_time=f_idle_time,
                                                    engine_time=f_engineTime,
                                                    )

            _day_summary_catalog = ret['daily'].get('overall')
            ret['daily']['overall'] = fill_metrics(_day_summary_catalog,
                                                   label=utils.ts2strday(timestamp, account.timeZone),
                                                   distance=f_distance,
                                                   economy=f_economy,
                                                   economy_count=f_economy_count,
                                                   accel=f_accel,
                                                   breaking=f_break,
                                                   motion=f_motion,
                                                   overspeed=f_overspeed,
                                                   steering=f_steering,
                                                   fuelUsed=f_fuelUsed,
                                                   idle_time=f_idle_time,
                                                   engine_time=f_engineTime,
                                                   )

        lastRow = row

    ## Relabel current frames
    _current_month_index = utils.ts2intmonth(current_month, account.timeZone)
    _current_week_index = utils.ts2intweek(current_week, account.timeZone)
    _current_day_index = utils.ts2intday(current_day, account.timeZone)

    if _current_month_index in ret['monthly']:
        ret['monthly'][_current_month_index]['label'] = "This Month"

    if _current_week_index in ret['weekly']:
        ret['weekly'][_current_week_index]['label'] = "This Week"

    if _current_day_index in ret['daily']:
        ret['daily'][_current_day_index]['label'] = "Today"

    ## Remove calculation Fields
    outFields = [
        'AccelGrade',
        'BreakGrade',
        'EconomyGrade',
        'EconomyScore',
        'FuelDistGrade',
        'FuelDistScore',
        'IdleGrade',
        'IdleScore',
        'SmoothStartsScore',
        'SmoothStopsScore',
        'SteeringScore',
        'SteeringGrade',
        'distance',
        'economy',
        'label',
        'OverallGrade',
        'OverallScore',
        'Co2Score',
        'Co2Grade',
        'Co2Value',
        '__fuelUsed',
        '__distance',
        'Co2Goal'
    ]

    for month_index in ret['monthly']:
        ret['monthly'][month_index] = dict(
            [(k, ret['monthly'][month_index][k]) for k in ret['monthly'][month_index] if k in outFields])

    for month_index in ret['weekly']:
        ret['weekly'][month_index] = dict(
            [(k, ret['weekly'][month_index][k]) for k in ret['weekly'][month_index] if k in outFields])

    for month_index in ret['daily']:
        ret['daily'][month_index] = dict(
            [(k, ret['daily'][month_index][k]) for k in ret['daily'][month_index] if k in outFields])

    ret['deviceID'] = deviceid
    service.logger.debug(ret)
    return ret


@service.priv_route('/v<version>/report/alerts', method=['GET', 'OPTIONS'])
@hooks.checksum(sha1)
def alerts(account, user, groups, db, version):
    if request.method == "OPTIONS": return

    ## Pull Records
    query = db.query(models.RuleTrigger, models.Rule) \
        .filter(models.RuleTrigger.accountID == account.accountID) \
        .filter(models.RuleTrigger.isActive == 1) \
        .filter(models.RuleTrigger.accountID == account.accountID) \
        .filter(models.RuleTrigger.ruleID == models.Rule.ruleID)

    alerts = []
    count = 0

    for alert, rule in query:
        count += 1
        loc = db.query(models.EventData) \
            .filter(models.EventData.accountID == account.accountID) \
            .filter(models.EventData.gps_timestamp < alert.lastTriggerTime) \
            .filter(models.EventData.deviceID == alert.deviceID) \
            .order_by(models.EventData.gps_timestamp.desc()) \
            .limit(1).first()

        alerts.append({
            'ruleID': alert.ruleID,
            'deviceID': alert.deviceID,
            'date': utils.ts2strdate_long(alert.lastTriggerTime, account.timeZone),
            'time': utils.ts2strtime_long(alert.lastTriggerTime, account.timeZone),
            'latitude': float(loc.latitude),
            'longitude': float(loc.longitude),
            'address': loc.address,
            'label': rule.description
        })

    return {'alerts': alerts, 'length': count}


@service.route('/v<version>/report/rss/theft', method=['GET', 'OPTIONS'])
@service.db
def theft_rss(db, version):
    if request.method == "OPTIONS": return

    desc = """
            <h1>iCar Theft Notification Alert: {DESC}</h1>
            <b>Flagged:</b> {FDATE}<br />
            
            <h2>Vehicle Information:</h2>
            <b>VIN:</b> {VIN}<br />
            <b>Year:</b> {YEAR}<br />
            <b>Make:</b> {MAKE}<br />
            <b>Model:</b> {MODEL}<br />
            
            <h2>Customer Information:</h2>
            <b>Account:</b> {ACCOUNT}<br />
            <b>Contact Name:</b> {NAME}<br />
            <b>Phone Number:</b> {NUMBER}<br />
            <b>Email Address:</b> {EMAIL}<br />
            <b>Time Zone:</b> {ZONE}<br />
    """

    rss = RSS2Feed(title='iCar Theft Recovery', description='All iCar vehicles flagged as stolen',
                   link=service.service_url + '/report/rss/theft')
    theft_records = db.query(models.DevicePublic, models.Device, models.Account) \
        .join(models.Device, models.DevicePublic.deviceID == models.Device.deviceID) \
        .join(models.Account, models.Account.accountID == models.Device.accountID) \
        .group_by(models.DevicePublic.uuid) \
        .filter(models.DevicePublic.type == 'THEFT')

    for theft, device, account in theft_records:
        rss.append_item(
            title=' - '.join([account.contactName, device.description]),
            link='http://api.skytrace.com/v3.0/report/location/{0}'.format(theft.uuid),
            description=desc.format(
                FDATE=utils.ts2strdatetime_long(theft.createTimestamp, account.timeZone),
                ACCOUNT=account.accountID,
                NAME=account.contactName,
                NUMBER=account.contactPhone,
                EMAIL=account.contactEmail,
                ZONE=account.timeZone,
                YEAR=device.vehicleYear,
                MAKE=device.vehicleMake,
                MODEL=device.vehicleModel,
                VIN=device.vehicleID,
                DESC=device.description)
        )

    response.set_header('Content-Type', 'text/xml')
    return rss.get_xml()


@service.route('/v<version>/report/location/<key>', method=['GET', 'OPTIONS'])
@service.db
def pub_location(key, db, version):
    if request.method == "OPTIONS": return

    """Public Location for shared devices: Web Frontend"""
    service.logger.debug("{0} GET: /location/{1}".format(version, key))

    theft_records = db.query(models.DevicePublic, models.Device, models.Account) \
        .join(models.Device, models.DevicePublic.deviceID == models.Device.deviceID) \
        .join(models.Account, models.Account.accountID == models.Device.accountID) \
        .group_by(models.DevicePublic.uuid) \
        .filter(models.DevicePublic.uuid == key) \
        .first()

    if not theft_records: abort(404, "Not Found");

    key_values = {'KEY': key}
    theft, device, account = theft_records

    eventData = db.query(models.EventData) \
        .filter(models.EventData.deviceID == theft.deviceID) \
        .filter(models.EventData.HDOP < 5) \
        .filter(models.EventData.latitude != 0) \
        .filter(models.EventData.longitude != 0) \
        .order_by(models.EventData.timestamp.desc()) \
        .first()
    if not eventData:
        mime_type, html = get_resource('report_location_not_found.html')
        return Template(html).render(key_values)

    key_values['ACCOUNTID'] = account.accountID
    key_values['CONTACTNAME'] = account.contactName
    key_values['CONTACTPHONE'] = account.contactPhone
    key_values['CONTACTEMAIL'] = account.contactEmail
    key_values['VEHICLEID'] = device.vehicleID
    key_values['YEAR'] = device.vehicleYear
    key_values['MAKE'] = device.vehicleMake
    key_values['MODEL'] = device.vehicleModel
    key_values['COLOR'] = device.vehicleColor

    key_values['LATITUDE'] = float(device.lastValidLatitude)
    key_values['LONGITUDE'] = float(device.lastValidLongitude)

    fuelEconomyMPG = float(device.fuelEconomy) / 0.4251
    key_values['MINTANK'] = round(float(device.fuelCapacity * device.lastFuelLevel) * fuelEconomyMPG, 2)

    mime_type, html = get_resource('report_location.html')
    return Template(html).render(key_values)


@service.route('/v<version>/report/location/a/<key>', method=['GET', 'OPTIONS'])
@service.db
def pub_getLocation(key, db, version):
    if request.method == "OPTIONS": return

    theft = db.query(models.DevicePublic) \
        .filter(models.DevicePublic.uuid == key) \
        .first()

    if not theft: abort(404, "Not Found");

    eventData = db.query(models.EventData) \
        .filter(models.EventData.deviceID == theft.deviceID) \
        .filter(models.EventData.HDOP < 5) \
        .filter(models.EventData.latitude != 0) \
        .filter(models.EventData.longitude != 0) \
        .order_by(models.EventData.timestamp.desc()) \
        .first()

    if not eventData: abort(404, "Device Not Found")

    age = utils.utc_timestamp() - eventData.gps_timestamp
    age_m = age / 60
    age_s = age % 60
    age_h = age_m / 60
    age_m = age_m % 60

    return {
        'latitude': float(eventData.latitude),
        'longitude': float(eventData.longitude),
        'heading': float(eventData.heading),
        'speed': round(float(eventData.speedKPH) / 1.609344, 2),
        'altitude': float(eventData.altitude),
        'street': eventData.streetAddress,
        'city': eventData.city,
        'state': eventData.stateProvince,
        'hdop': float(eventData.HDOP),
        'ignition': int(eventData.engineRpm) > 0,
        'age': "{0}h {1}m {2}s".format(age_h, age_m, age_s),
    }


@service.priv_route('/v<version>/report/pushServices', method=['POST', 'OPTIONS'])
@hooks.checksum(sha1)
def pushServices(account, user, groups, db, version):
    if request.method == "OPTIONS": return

    service.logger.debug("{0} POST: /report/pushServices".format(version))
    isExistingUUID = db.query(models.PushNotificationRegID) \
        .filter(models.PushNotificationRegID.deviceUUID == request.json['pushTokenDetails']['deviceUUID']) \
        .first()

    if isExistingUUID:
        db.query(models.PushNotificationRegID) \
            .filter(models.PushNotificationRegID.deviceUUID == request.json['pushTokenDetails']['deviceUUID']) \
            .update(request.json['pushTokenDetails'])

    else:
        newMobileDevice = models.PushNotificationRegID()
        newMobileDevice.userID = user.userID
        newMobileDevice.devicePlatform = request.json['pushTokenDetails']['devicePlatform']
        newMobileDevice.registrationId = request.json['pushTokenDetails']['registrationId']
        newMobileDevice.deviceUUID = request.json['pushTokenDetails']['deviceUUID']

        try:
            db.add(newMobileDevice)
        except errors.IntegrityError:
            db.rollback()
            abort(409, 'Failed to add the mobile device.')

    try:
        db.commit()
    except errors.IntegrityError:
        db.rollback()
        abort(409, 'Failed to commit the mobile device data.')

    return {"statusCode": 200, "message": "Successfully added/updated"}


@service.route('/v<version>/report/pushNotification', method=['POST', 'OPTIONS'])
@service.db
def processPushNotification(db, version):
    if request.method == "OPTIONS": return

    service.logger.debug("{0} POST: /report/pushNotification".format(version))

    headers = {
        "Authorization": "key=" + service.FCM_ApiKey,
        "project_id": service.FCM_projectId,
        "Content-Type": "application/json",
        "Host": service.FCM_host,
        "Content-Length": 534,
        "Cache-Control": "no-cache"
    }

    account = db.query(models.Account) \
        .filter(models.Account.accountID == request.json['accountID']) \
        .first()

    query = db.query(models.PushNotificationRegID) \
        .filter(models.PushNotificationRegID.userID == request.json['accountID'])

    def post_to_FCM(row, payload):
        res = requests.post(service.FCM_url, data=json.dumps(payload), headers=headers)
        content = json.loads(res.content)
        if content['success'] == 0:
            row.lastError = content['results'][0]['error']
            row.lastErrorCount += 1
        else:
            row.lastError = ''
            row.lastErrorCount = 0
        try:
            db.commit()
        except Exception as ex:
            db.rollback()
            abort(409, 'Failed to commit the mobile device data.')

    for row in query:
        if request.json['type'] == "overspeed" and account.overspeedAlert:
            threshold_speed = db.query(models.Device) \
                .with_entities(models.Device.speedLimitKPH) \
                .filter(models.Device.accountID == account.accountID) \
                .first().speedLimitKPH
            if threshold_speed is 0 or request.json['speedKPH'] >= threshold_speed:
                if row.devicePlatform == 'iOS':
                    data = {
                        "type": "overspeed",
                        "speedKPH": float(request.json['speedKPH']),
                        "time": int(request.json['timestamp'])
                    }

                    payload = {
                        "data": data,
                        "notification": {
                            "title": "Mycar Overspeed Alert",
                            "body": "There is a new overspeed alert from your car.",
                            "sound": "Default"
                        },
                        "priority": "high",
                        "registration_ids": [row.registrationId]
                    }
                    post_to_FCM(row, payload)
                else:
                    data = {
                        "title": "Mycar Overspeed Alert",
                        "body": "There is a new overspeed alert from your car.",
                        "type": "overspeed",
                        # "address": str(request.json['address']),
                        "speedKPH": float(request.json['speedKPH']),
                        "time": int(request.json['timestamp']),
                        "notId": random.randint(1111, 9999)
                    }

                    payload = {
                        "data": data,
                        "priority": "high",
                        "registration_ids": [row.registrationId]
                    }
                    post_to_FCM(row, payload)
        elif request.json['type'] == "geozone" and account.geofencingAlert:
            geozone = db.query(models.Geozone) \
                    .filter(models.Geozone.geozoneID == request.json['geozoneID']) \
                    .first()

            if row.devicePlatform == 'iOS':
                data = {
                    "type": "geozone",
                    "geozone": str(geozone.displayName),
                    "time": int(request.json['timestamp'])
                }

                payload = {
                    "data": data,
                    "notification": {
                        "title": "Mycar Geofencing Alert",
                        "body": "Your car just crossed one of the boundaries set by you!",
                        "sound": "Default"
                    },
                    "priority": "high",
                    "registration_ids": [row.registrationId]
                }
                post_to_FCM(row, payload)
            else:
                data = {
                    "title": "Mycar Geofencing Alert",
                    "body": "Your car just crossed one of the boundaries set by you!",
                    "type": "geozone",
                    "geozone": str(geozone.displayName),
                    "time": int(request.json['timestamp']),
                    "notId": random.randint(1111, 9999)
                }

                payload = {
                    "data": data,
                    "priority": "high",
                    "registration_ids": [row.registrationId]
                }
                post_to_FCM(row, payload)
        elif request.json['type'] == "milcode" and account.milAlert:
            if row.devicePlatform == 'iOS':
                data = {
                    "type": "milcode",
                    "faultCode": request.json['faultCode'],
                    "time": int(request.json['timestamp'])
                }

                payload = {
                    "data": data,
                    "notification": {
                        "title": "Mycar MIL fault Alert",
                        "body": "Engine malfunctioning alert from your car.",
                        "sound": "Default"
                    },
                    "priority": "high",
                    "registration_ids": [row.registrationId]
                }
                post_to_FCM(row, payload)
            else:
                data = {
                    "title": "Mycar MIL fault Alert",
                    "body": "Engine malfunctioning alert from your car.",
                    "type": "milcode",
                    "faultCode": request.json['faultCode'],
                    "time": int(request.json['timestamp']),
                    "notId": random.randint(1111, 9999)
                }

                payload = {
                    "data": data,
                    "priority": "high",
                    "registration_ids": [row.registrationId]
                }
                post_to_FCM(row, payload)