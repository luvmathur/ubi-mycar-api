
def driver_score(engine_time, events):
    """Return driver score according to event count per 15 minute time-block where 0-1 event is a perfect score
    and 16 or higher is a zero. A 16 instead of 15 is used so the range still spans 15 numbers depreciating evenly
    from 1 event at 100% down to 16 at 0%.
    For inverted_score a -1 is used so an event count of 1 still returns a perfect score.

    :param int engine_time: drive time in seconds
    :param int events: driving infraction counter for driver scoring
    :rtype: float
    """
    if engine_time <= 0:
        return
    engine_time_in_minutes = engine_time / 60.0
    time_blocks = engine_time_in_minutes / 60.0
    events_per_block = events / time_blocks
    if events_per_block <= 1.0:
        return 1.0
    elif events_per_block >= 16:
        return 0.0
    inverted_score = (events_per_block - 1) / 15.0
    drive_score = 1 - inverted_score
    return drive_score
