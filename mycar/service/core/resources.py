from threading import Lock
from mimetypes import guess_type
from os.path import basename, join, isfile
from os import listdir

resource_dir = './static'
mutex = Lock()
file_cache = {}
file_mime = {}

with mutex:
    file_paths = dict([ (f, join(resource_dir,f)) for f in listdir(resource_dir) if isfile(join(resource_dir, f)) ])

def _load_resource(key):
    _filepath = file_paths[key]
    with mutex:
        file_mime[key], _ = guess_type(_filepath)
        with open(_filepath, 'rb') as fd:
            file_cache[key] = fd.read()

def has_resource(res):
    return file_paths.has_key(res)

def get_resource(res):
    if not file_cache.has_key(res):
        _load_resource(res)
    return file_mime[res], file_cache[res]

