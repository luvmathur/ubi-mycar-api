from core import abort, response
from stslib.database import exists
import json

def inrole(roles):
    """Test if the current user is one of the provided roles.
    
    Arguments:
        roles - A iterable list of roles to test against user.roleID
    """
    if type(roles) is str: roles = [roles]
    def apply(callback):
        def _wrapper(*args, **kwargs):
            if 'user' in kwargs and kwargs['user'].roleID in roles:
                return callback(*args, **kwargs)
            abort(403, "Incificiate Authority")
        return _wrapper
    return apply


def validateid(model_key, arg_key):
    """Decorator that verifies an entity exists before allowing entry to the function

    If the entity does nto exist, the request is aborted with a 400 code

    Arguments:
        model_key - The field to look for the value in any given model. ie: models.User.userID
        arg_key - the name of the paramater that holds the vaule.
    """
    def apply(callback):
        def _wrapper(*args, **kwargs):
            id=kwargs[arg_key]
            db=kwargs['db']
            if not db.query(exists().where(model_key==id)).scalar():
                abort(400, 'Resource does not exist')
            return callback(*args, **kwargs)
        return _wrapper
    return apply


def checksum(algorithm):
    """Decorator that adds a hash of all the data returned by the function to which it is applied

    The data returned must be a dictionary object and must NOT contain a value for key: 'checksum'

    Arguments:
        algorithm - A pointer to the hashlib algorithm used to create the checksum.
                    Example: hashlib.sha1
    """
    def _apply(callback):
        def _wrapper(*args, **kwargs):
            hasher = algorithm()
            data = callback(*args, **kwargs)
            hasher.update(json.dumps(data))
            data['checksum'] = hasher.hexdigest()
            return data
        return _wrapper
    return _apply

def asStatusEntity():
    def _apply(callback):
        def _wrapper(*args, **kwargs):
            data = callback(*args, **kwargs)
            return {
                'code' : response.status_code, 
                'status' : response.status, 
                'message' : data 
            }
        return _wrapper
    return _apply
