import hmac as _hmac
import base64
import hashlib
from time import time
from datetime import datetime
from pytz import timezone
import math
import uuid
import pbkdf2

def pack(obj, keys, **kwargs):
    """Copy a dictionary from a iterable list of keys

    Arguments:
        obj   - The dictionary containing the values to copy from
        keys  - A iterable list of keys that will define the items
                to extract from 'obj'

    Keyword Arguments:
        normalize   - If true, all keys will be returned in the new
                      object regardless of if they exist in the source
                      data. DEFAULT: True
    """
    kwargs.setdefault('normalize', True)

    _obj = obj
    if type(obj) is not dict:
        _obj = obj.__dict__

    if not kwargs['normalize']:
        set_source = set(k for k in _obj)
        set_keys = set(k for k in keys)
        keys = set_source.intersection(set_keys)
       
    return dict((k,_obj.get(k)) for k in keys)

def getRadialDistance(p1, p2):
    R = 6371
    _lat1, _lon1 = p1
    _lat2, _lon2 = p2

    dLat = math.radians(_lat2 - _lat1)
    dLon = math.radians(_lon2 - _lon1)

    a = math.sin(dLat/2) * math.sin(dLat/2) +\
        math.cos(math.radians(_lat1)) * math.cos(math.radians(_lat2)) *\
        math.sin(dLon/2) * math.sin(dLon/2)

    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
    return R * c # Km

def utc_timestamp():
    return int(time())

def hmac(key, message):
    return _hmac.new(key, message, hashlib.sha1).hexdigest()

def hashpw(key, message):
    return _hmac.new(key, message, hashlib.sha256).hexdigest()

#def hmac(key, message):
#    return base64.b64encode(_hmac.new(key, message, hashlib.sha1).digest())


def generate_new_salt():
    return str(uuid.uuid4()).replace('-', '')


def compute_salted_password(stage1, salt):
    return pbkdf2.PBKDF2(stage1, salt, 1000).read(20).encode('hex')


def timestamp2tztime(ts, tz):
    return datetime.fromtimestamp(ts, timezone(tz))


def ts2strtime_short(ts, tz):
    return timestamp2tztime(ts, tz).strftime('%X')

def ts2strtime_short2(ts, tz):
    return timestamp2tztime(ts, tz).strftime('%I:%M%P')

def ts2strtime_long(ts, tz):
    return timestamp2tztime(ts, tz).strftime('%I:%M:%S %p')

def ts2strdate_short(ts, tz):
    return timestamp2tztime(ts, tz).strftime('%x')

def ts2strdate_short2(ts, tz):
    return timestamp2tztime(ts, tz).strftime('%m/%d')

def ts2strdate_long(ts, tz):
    return timestamp2tztime(ts, tz).strftime('%b %d, %Y')

def ts2strdatetime_long(ts, tz):
    return timestamp2tztime(ts, tz).strftime('%c')

def ts2strmonth(ts, tz):
    return timestamp2tztime(ts, tz).strftime('%b')

def ts2strweek(ts, tz):
    return timestamp2tztime(ts, tz).strftime('Week %U')

def ts2strday(ts, tz):
    return timestamp2tztime(ts, tz).strftime('%a')

def ts2intmonth(ts, tz):
    return timestamp2tztime(ts, tz).strftime('%y%m')

def ts2intweek(ts, tz):
    return timestamp2tztime(ts, tz).strftime('%y%U')

def ts2intday(ts, tz):
    return timestamp2tztime(ts, tz).strftime('%y%j')

class UnitConverter(object):
    def __init__(self, account):
        self._speed = [
            ( 'mph', 'Miles per Hour', self.kmph2mph ),
            ( 'kmph', 'Kilometers per Hour', self.kmph2kmph ),
            ( 'Km', 'Knots', self.kmph2km ),
        ]
        
        self._distance = [
            ( 'miles', 'Miles', self.km2miles ),
            ( 'km', 'Kilometers', self.km2km ),
            ( 'NM', 'Knotical Miles', self.km2nm )
        ]

        self._altitude = [
            ( 'ft', 'feet', self.m2ft ),
            ( 'm', 'meters', self.m2m ),
            ( 'm', 'meters', self.m2m ),
        ]

        self._volume = [
            ('gal', 'US Gallons', self.l2gal),
            ('L', 'Liters', self.l2l),
            ('IG', 'Imperial Gallons', self.l2ig),
            ('cft', 'Cubic Feet', self.l2cft),
        ]

        self._economy = [
            ('mpg', 'Miles per Gallon', self.kml2mpg),
            ('km/L', 'Kilometers per Liter', self.kml2kml ),
            ('kmg', 'Kilometers per Gallon', self.kml2kmg ),
            ('L/100km' 'Liters per 100 Kilometers', self.kml2L_100km),
        ]

        self._pressure = [
            ('kPa', 'Kilopascals', None),
            ('psi', 'Pounds per Square Inch', None),
            ('bar', 'bars', None),
        ]

        self._temperature = [
            ('F', 'Degrees Fahrenkeit', self.f2c),
            ('C', 'Degrees', self.c2c),
        ]

        self.set_units(
            speed=account.speedUnits if account.speedUnits else 0,
            distance=account.distanceUnits if account.distanceUnits else 0,
            altitude=account.distanceUnits if account.distanceUnits else 0,
            volume=account.volumeUnits if account.distanceUnits else 0,
            pressure=account.pressureUnits if account.pressureUnits else 0,
            economy=account.economyUnits if account.economyUnits else 0,
            temperature=account.temperatureUnits if account.temperatureUnits else 0
        )

    def set_units(self, **kwargs):
        """Generate locals functions for speed, distance, volumn, economy, pressure, and temperature

        Keyword Arguments: 
        Provide the Newmeric offset identifing the conversion method for each unit transformation.
            speed - magnitude of velocity, rate of change in position
            distance - proximity of 2 objects
            distance - proximity of 1 object relative to sea level.
            volume - amount of space occupied by a substance
            pressure - Ratio of force to area
            economy - efficiency of the process of converting chemical energy to kinetic energy
            temperature - relation between hot and cold
        """

        self.speed_symbol, self.speed_label, self.speed2local = self._speed[kwargs['speed']]
        self.distance_symbol, self.distance_label, self.distance2local = self._distance[kwargs['distance']]
        self.altitude_symbol, self.altitude_label, self.altitude2local = self._altitude[kwargs['altitude']]
        self.volume_symbol, self.volume_label, self.volume2local = self._volume[kwargs['volume']]
        self.pressure_symbol, self.pressure_label, self.pressure2local = self._pressure[kwargs['pressure']]
        self.economy_symbol, self.economy_label, self.economy2local = self._economy[kwargs['economy']]
        self.temperature_symbol, self.temperature_label, self.temperat2local = self._temperature[kwargs['temperature']]

    ##speed
    def kmph2mph(self, value):
        """mph - Miles per Hour """
        return float(value) * 0.621371

    def kmph2kmph(self, value):
        """kmph - Kilometers per Hour """
        return float(value)

    def kmph2km(self, value):
        """Km - Knots """
        return float(value) * 0.539957

        
    ##distance
    def km2miles(self, value):
        """miles - Miles """
        return float(value) * 0.621371

    def km2km(self, value):
        """km - Kilometers """
        return float(value)

    def km2nm(self, value):
        """NM - Knotical Miles """
        return float(value) * 0.539957

    ##altitutde
    def m2ft(self, value):
        """ft - Feet """
        return float(value) * 3.28084

    def m2m(self, value):
        """m - meters """
        return float(value)

    ##volume
    def l2gal(self, value):
        """gal - US Gallons """
        return float(value) * 0.264172

    def l2l(self, value):
        """L - Liters """
        return float(value)

    def l2ig(self, value):
        """IG - Imperial Gallons """
        return float(value) * 0.219969

    def l2cft(self, value):
        """cft - Cubic Feet """
        return float(value) * 0.0353147


    ##economy
    def kml2mpg(self, value):
        """mpg - Miles per Gallon """
        return float(value) * 2.35215

    def kml2kml(self, value):
        """km/L - Kilometers per Liter """
        return float(value)

    def kml2kmg(self, value):
        """kmg - Kilometers per Gallon
        This might not be correct. Depends on if Gallons refrences UK or US.
        Currently it is UK
        """
        return float(value) * 4.54609188

    def kml2L_100km(self, value):
        """L/100km Liters per 100 Kilometers - """
        return 100.0 / float(value)

    ##pressure
        ## NOT IMPLEMENTED

    ##temprature
    def f2c(self, value):
        """F - Degrees Fahrenkeit"""
        return float(value) * 9 / 5 + 32

    def c2c(self, value):
        """C - Degrees"""
        return float(value)

def avg(lst):
    return sum(lst) / len(lst)
 
def basic_linear_regression(x, y):
    # Basic computations to save a little time.
    length = len(x)
    sum_x = sum(x)
    sum_y = sum(y)

    # ?x^2, and ?xy respectively.
    sum_x_squared = sum(map(lambda a: a * a, x))
    covariance = sum([x[i] * y[i] for i in range(length)])

    # Magic formulae!  
    a = (covariance - (sum_x * sum_y) / length) / (sum_x_squared - ((sum_x ** 2) / length))
    b = (sum_y - a * sum_x) / length
    return a, b

