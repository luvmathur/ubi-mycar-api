from os.path import basename, join, isfile
from os import listdir
from smtplib import SMTP
from message import EmailMessage
from quik import Template
from BeautifulSoup import BeautifulSoup
from socket import gethostname

__all__ = ['EmailMessage', 'load_template']


def load_template(html_path, text_path, resource_dir, params):
    """ Create a EmailMessage from a template.

    This will generate a html and text message from the 2 templates provided
    as well as load CID resource from the provided resource directory. The
    returned message will need to have the Subject, To, From, Cc, and Bcc 
    attributes set.

    Arguments:
        html_path    - The file path to the html template
        text_path    - The file path to the text template
        resource_dir - The direcotry where the image CID resources are stored
        params       - Provided key value pairs to fill the templates provided.
    
    Returns:
        A EmailMessage generated from the provided args.
    """

    ## Load and render the html content
    with open(html_path) as fd:
        html_content = Template(fd.read()).render(params)

    ## Load and render the text content
    with open(text_path) as fd:
        text_content = Template(fd.read()).render(params)

    ## Create the email message from the rendered content
    msg = EmailMessage([
        (html_content, 'html'),
        (text_content, 'text'),
    ])

    ## load list of all available resource
    cids = dict([ (f, join(resource_dir,f)) for f in listdir(resource_dir) if isfile(join(resource_dir, f)) ])

    ## Load CID resources
    parser = BeautifulSoup(html_content)
    for img_tag in parser.findAll('img'):
        cid_tag, src = img_tag.get('src').split(':')
        if cid_tag == 'cid' and cids.has_key(src):
            msg.attach_cid_image(cids[src], src)

    return msg


class SmtpServer():
    def __init__(self, resource, credentials):
        self._resource = resource
        self._credentials = credentials

        

    def connect_and_send(self,message):
        un, pw = self._credentials
        try:
            self._smtp = SMTP()
            self._smtp.connect(self._resource)
            self._smtp.starttls()
            # Postfix mail relay does not require authentication
            # self._smtp.login(un,pw)
            self._smtp.helo(gethostname())
            self._smtp.sendmail(message.get_from(), 
                                message.get_to(), 
                                message._msg.as_string())
        except Exception as ex:
            return False
        finally:
            self._smtp.close()

        return True


