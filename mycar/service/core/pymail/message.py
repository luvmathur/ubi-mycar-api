from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email.mime.image import MIMEImage
from mimetypes import guess_type
from base64 import b64encode
from os.path import basename

class EmailMessage():
    def __init__(self, body=None, **kwargs):
        """ Create a new Email Message

        Optional Arguments:
            body - a list of tuples defining the body of the message

        Keyword Arguments:
            From     - The email from which the email is originating
            To       - A list of emails to which the emails are going
            Subject  - The subject of the email
        """
        self._msg = MIMEMultipart('related')
        self._body = MIMEMultipart('alternative')
        self._msg.attach(self._body)

        if body : self.set_body(body)
        if kwargs.has_key('From'): self.set_from(kwargs['From'])
        if kwargs.has_key('To'): self.set_to(kwargs['To'])
        if kwargs.has_key('Cc'): self.set_cc(kwargs['Cc'])
        if kwargs.has_key('Bcc'): self.set_bcc(kwargs['Bcc'])
        if kwargs.has_key('Subject'): self.set_subject(kwargs['Subject'])


    def set_body(self, body):
        """Set the body of the email with html and alternate text

        Example:
            [ ('This is a text message', 'text'),
              ('<b>This is a html message<b>', 'html') ]

        Arguments:
            body - a list of tuples defining the body of the message
        """
        for data, _type in body:
            self._body.attach(MIMEText(data, _type))


    def set_from(self, frm):
        """ Set the sender of the email

        Arguments:
            frm - The originating email address
        """
        self._from = frm
        self._msg['From' ] = frm


    def get_from(self):
        """ Retrieve the originating address for this email"""
        return self._from

        
    def set_to(self, to):
        """ Set the recipient of the email

        This can be a list of email address

        Arguments:
            to  - A list of emails to which the emails are going
        """
        if not type(to) is list: to = [to]

        _to = ', '.join(to)
        self._to = to
        self._msg['To'] = _to


    def get_to(self):
        """ Retrieve the destination address for this email"""
        return self._to


    def set_subject(self, subject):
        """ Set the subject of the email

        Arguments:
            subject - The subject of the email
        """
        self._msg['Subject'] = subject


    def get_subject(self):
        """ Retrieve the subject of the email"""
        return self._msg.get('Subject')


    def attach_raw(self, data, mime_type='text/plan', filename=None):
        """ Attach a file to the email as a set of raw bytes

        Arguments:
            data - The data for the email body

        Optional Arguments:
            mime_type - The type of data. (default: text/plain)
            filename  - The file disposition for the attachment
        """
        #type, subtype = mime_type.split('/')
        _a= MIMEBase(*(mime_type.split('/')))
        _a.set_payload(b64encode(data))
        _a.add_header('Content-Transfer-Encoding', 'base64')
        _a.add_header('content-Disposition', 'attachment', 
                      filename=filename if filename else 'attachment')
        self._msg.attach(_a)
    

    def attach_file(self, path, mime_type=None, filename=None):
        """ Attach a file to the email from a file on the disk

        Arguments:
            path - The path to the file.

        Optional Arguments:
            mime_type - Override the detected mime type
            filename  - Override the file disposition
        """
        if not mime_type:
            mime_type, _ = guess_type(path)
            if not mime_type:
                mime_type = 'application/octet-stream'

        if not filename:
            filename = basename(path)

        with open(path, 'rb') as fd:
            self.attach_raw(fd.read(), mime_type, filename)


    def attach_cid_image(self, path, cid=None):
        """ Attach a image as IDed content that will be displayed in the message body

        Arguments:
            path - The path to the resource
            cid  - the content-id name that will be assigned to the sources
        """

        if not cid: cid = basename(path)
      
        with open(path, 'rb') as fd:
            img = MIMEImage(fd.read())
            img.add_header('Content-ID', ''.join(['<',cid,'>']))
            self._msg.attach(img)


