import base64
import json
from bottle import Bottle
from stslib.database import connect, Session
from core.pymail import SmtpServer, load_template
import utils

from .logger import Logger

try: ## python version issues with 3.3
    from ConfigParser import SafeConfigParser
except ImportError:
    from configparser import SafeConfigParser

## Extend access to specific bottle functions and properties
from bottle import request, response, abort, HTTP_CODES, static_file


class WebService(Bottle):
    def __init__(self, name, config_path, catchall=False, autojson=True):
        self.name = name
        super(WebService, self).__init__(catchall, autojson)
        print("i am here.......... ")
        print(config_path)
        self.load_config(config_path)


        @self.hook('after_request')
        def enable_cors():
            response.headers['Access-Control-Allow-Origin'] = '*'
            response.headers['Access-Control-Allow-Methods'] = 'GET, POST, PUT, OPTIONS, DELETE'
            response.headers['Access-Control-Allow-Headers'] = 'authority, Content-Type'

        @self.error(400)
        @self.error(401)
        @self.error(402)
        @self.error(403)
        @self.error(404)
        @self.error(409)
        @self.error(410)
        @self.error(500)
        @self.error(501)
        def _err(resp, **kwargs):
            self.logger.debug('*** RECIEVED ERROR ***')
            self.logger.debug(resp.__dict__)
            enable_cors()
            return json.dumps({ 'Code' : resp.status_code, 'Status' : resp.status, 'Reason' : resp.body })

    def send_activation(self, user_name, email, activation_key):
        """Send the activation email to the given email address

        Arguments:
            email - The email to send the activation link to.
            activation_key - The key used to activate the account.
        """
        self.logger.debug("Sending Activation Email:")
        self.logger.debug("TO: %s" % email)
        self.logger.debug("KKEY: %s" % activation_key)

        msg = load_template(
            './static/email_activate.html', 
            './static/email_activate.txt', './static/', 
            {
                'user' : user_name,
                'key' : activation_key
            }
        )

        msg.set_to(email)
        msg.set_from(self.my_email)
        msg.set_subject('Welcome To myCar!')

        self.smtp.connect_and_send(msg)

    def send_pending_activation(self, email, activation_key):
        """Send activation email to the given email address
           This is for the new user sign up
        Arguments:
            email - The email to send the activation link to.
            activation_key - The key used to activate the account.
        """
        self.logger.debug("Sending Activation email for new myCar signup:")
        self.logger.debug("TO: %s" % email)
        self.logger.debug("KKEY: %s" % activation_key)

        msg = load_template(
            './static/email_pending_activation.html',
            './static/email_pending_activation.txt', './static/',
            {
                'key' : activation_key
            }
        )

        msg.set_to(email)
        msg.set_from(self.my_email)
        msg.set_subject('Welcome to myCar!')

        self.smtp.connect_and_send(msg)
    def send_forgot_password(self, user_name, email, activation_key):
        """Send the password reset email to the given email address

        Arguments:
            email - The email to send the activation link to.
            activation_key - The key used to activate the account.
        """
        self.logger.debug("Sending Password Reset Email:")
        self.logger.debug("TO: %s" % email)
        self.logger.debug("KKEY: %s" % activation_key)

        msg = load_template(
            './static/email_password.html', 
            './static/email_password.txt', './static/', 
            {
                ##'user' : user_name,
                'key' : activation_key
            }
        )

        msg.set_to(email)
        msg.set_from(self.my_email)
        msg.set_subject('Password Reset')

        self.smtp.connect_and_send(msg)

    def send_share_device(self, email, guid):
        """Send the share email link to the provided email

        Arguments:
            email - the email to send the share notice to.
            guid - the key used to access the data that has been shared
        """
        self.logger.debug("Share Device Telementry Email Sent:")
        self.logger.debug("TO: %s" % email)
        self.logger.debug("KKEY: %s" % guid)
        

    def load_config(self, path):
        self.config_file = SafeConfigParser()
        
        with open(path) as config_fid:
            self.config_file.readfp(config_fid)

        self.service_url = self.config_file.get('DEFAULT', 'url')
        self.service_version = self.config_file.get('DEFAULT', 'version')
        print(self.service_url)

        self.edmundsApiKey = self.config_file.get('Edmunds', 'api_key')
        self.FCM_ApiKey = self.config_file.get('Push', 'api_key')
        self.FCM_projectId = self.config_file.get('Push', 'project_id')
        self.FCM_host = self.config_file.get('Push', 'host')
        self.FCM_url = self.config_file.get('Push', 'url')
        
        smtp_server = self.config_file.get('Smtp', 'server')
        smtp_username = self.config_file.get('Smtp', 'username')
        smtp_password = self.config_file.get('Smtp', 'password')
        smtp_fromEmail = self.config_file.get('Smtp', 'fromEmail')
        smtp_fromName = self.config_file.get('Smtp', 'fromName')
        self.smtp = SmtpServer(smtp_server, (smtp_username, smtp_password))
        self.my_email = "{} <{}>".format(smtp_fromName, smtp_fromEmail)

        self._db_engine = connect(
            user=self.config_file.get("Database", "user"),
            password=self.config_file.get("Database", "password"),
            host=self.config_file.get("Database", "host"),
            port=self.config_file.getint("Database", "port"),
            database=self.config_file.get("Database", "database")
        )

        log_kwargs = dict() 
        if 'level' in self.config_file.options('Log'):
            log_kwargs['level'] = self.config_file.get('Log', 'level')

        if 'fmt' in self.config_file.options('Log'):
            log_kwargs['fmt'] = self.config_file.get('Log', 'fmt')

        if 'datefmt' in self.config_file.options('Log'):
            log_kwargs['datefmt'] = self.config_file.get('Log', 'datefmt')

        if 'dir' in self.config_file.options('Log'):
            self.log_path = '/'.join([self.config_file.get('Log', 'dir'), self.name])
            log_kwargs['fid'] = open(self.log_path, 'w+')
        else:
            self.log_path = None

        self.logger = Logger(self.service_version, **log_kwargs)

        self.logger.error("Application Startup: %s" % self.service_version)
        self.logger.error("Listing on URL: %s" % self.service_url)


    def priv_route(self, path, method=None, callback=None):
        """ decorator to bind a function to a protected request URL. Example::
    
            @app.priv_route('/hello/:name')
            def hello(name):
                return 'Hello %s' % name
    
        The ``:name`` part is a wildcard. See :class:`Router` for syntax
        details.
        """
        if not method:
            method = ['GET', 'OPTIONS']
        elif type(method) is list and 'OPTIONS' not in method:
            method.append('OPTIONS')
        elif type(method) is str and method != 'OPTIONS':
            method = [method, 'OPTIONS']

        return super(WebService, self).route(path, method, callback, apply=self._authority_plugin)

    def db(self, callback):
        def _wrapper(*args, **kwargs):
            db = Session(self._db_engine)
            kwargs['db'] = db
            data = callback(*args, **kwargs)
            db.dispose()
            return data
        return _wrapper

    @staticmethod
    def _get_authority():
        if 'authority' not in request.headers:
            abort(401, "No Authority Provided")

        try:
            decoded_auth = base64.b64decode(request.headers['authority'])
        except TypeError:
            abort(401, "Invalid Authority")

        # use rsplit here since the user name can contain colons
        user, passhash = decoded_auth.rsplit(':', 1)
        return {
            'userID': user,
            'passhash': passhash,
        }

    def _authority_plugin(self, callback):
        def _wrapper(*args, **kwargs):
            if request.method == "OPTIONS":
                return

            authority = self._get_authority()

            db = Session(self._db_engine)
            user = db.getUser(userID=authority['userID'])

            # Verify user exists
            if not user:
                abort(401, "Invalid Authority")

            # Verify user active
            if not user.isActive:
                abort(401, "Invalid Authority")

            # verify account active
            account = db.getAccount(user.accountID)
            if not account.isActive:
                abort(402, "Account Locked, Payment Required")

            # compute stage 2 password
            salt = user.salt or ''
            password = utils.compute_salted_password(authority['passhash'], salt)
            print("i am here...")
            print(password)
            print(user.password)
            # verify password
            if password != user.password:
                abort(401, "Invalid Authority umang fazi")

            # add resource to kwargs
            kwargs['db'] = db
            kwargs['user'] = user
            kwargs['account'] = account
            kwargs['groups'] = [x.groupID for x in db.getGroupsForUser(user)]

            data = callback(*args, **kwargs)
            db.dispose()
            return data
        return _wrapper


