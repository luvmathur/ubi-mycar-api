#!/bin/env python2

import time
import json
import logging
from urllib2 import Request, urlopen, HTTPError

logger = logging.getLogger("edmunds")

class EdmundsAPI(object):
    """
    Encapsulates querying VIN details from the Edmunds API
    """

    API_URL = 'https://api.edmunds.com/v1/api/toolsrepository/vindecoder?vin={vin}&fmt=json&api_key={key}'

    def __init__(self, key):
        self.api_key = key

    @staticmethod
    def _parse_api_obj(obj):
        """
        Takes an Edmunds API dataset and parses out information into a condensed format

        :param obj: dict
        :return: dict
        """
        attrib = obj.get('attributeGroups', {})
        categories = obj.get('categories', {})

        if attrib:
            attrib = attrib.get('SPECIFICATIONS', {})
        if attrib:
            attrib = attrib.get('attributes', {})

        mpg = attrib.get('EPA_COMBINED_MPG', {}).get('value')
        if not mpg:
            mpg = attrib.get('EPA_HIGHWAY_MPG', {}).get('value')
        if not mpg:
            mpg = attrib.get('EPA_CITY_MPG', {}).get('value')
        if not mpg:
            mpg = attrib.get('EGE_HIGHWAY_MPG', {}).get('value')
        if not mpg:
            mpg = attrib.get('EGE_CITY_MPG', {}).get('value')

        MPG_TO_KPL = 0.4251
        economy = float(mpg) * MPG_TO_KPL if mpg else 0

        USGAL_TO_L = 3.78541178
        fuel_capacity_gals = attrib.get('FUEL_CAPACITY', {}).get('value', 0)
        fuel_capacity = float(fuel_capacity_gals) * USGAL_TO_L if fuel_capacity_gals else 0

        return {
            'year': obj.get('year', ''),
            'make': obj.get('makeName', ''),
            'model': obj.get('modelName', ''),
            'name': obj.get('name', ''),
            'transmission': obj.get('transmissionType', ''),
            'economy_kpl': economy or '',
            'fuel_capacity_l': fuel_capacity or '',
            'vehicle_size': ' '.join(categories.get('Vehicle Size', [])),
            'vehicle_type': ' '.join(categories.get('Vehicle Type', [])),
            'vehicle_style': ' '.join(categories.get('Vehicle Style', [])),
            'market': ' '.join(categories.get('Market', [])),
        }

    def _query_api(self, vin):
        """
        Queries the Edmunds API for details about a VIN

        :param vin: string
        :return: (int, dict)
        """
        record = {}
        code = 0
        res = None

        req = Request(self.API_URL.format(vin=vin, key=self.api_key))
        try:
            res = urlopen(req)
            record = json.loads(res.read())['styleHolder'][0]
            code = res.getcode()
        except HTTPError as e:
            code = e.code
        except:
            pass
        finally:
            if res:
                res.close()

        return code, record

    def get_details(self, vin):
        """
        Queries Edmunds for details about a VIN and returns the http status code of the query
        and the details in a condensed format.

        :param vin: string
        :return: (int, dict)
        """
        response = {}
        code = 0

        for i in range(0,2):
            try:
                code, result = self._query_api(vin)
                if 200 >= code < 300:
                    response = self._parse_api_obj(result)
                    break
                else:
                    time.sleep(1)
            except:
                pass

        return code, response