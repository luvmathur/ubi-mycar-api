import logging
import sys

class Logger(logging.Logger,object):
    _levels = {
        'DEBUG' : logging.DEBUG,
        'INFO' : logging.INFO,
        'WARNING' : logging.WARNING,
        'ERROR' : logging.ERROR,
        'CRITICAL' : logging.CRITICAL,
    }

    def __init__(self, name, **kwargs):
        """Create instance of logger

        Keyword Arguments:
            level - Specify the log level. Default: 'INFO'
              fid - Where to write log data. Default: 'STDOUT'
              fmt - The format to use while writing to log.
           datefmt - The format of the date on the output.
        """

        kwargs.setdefault('fid', sys.stdout)
        kwargs.setdefault('level', 'INFO')
        kwargs.setdefault('fmt', '[%(asctime)s] (%(levelname)s) %(message)s')
        kwargs.setdefault('datefmt', '%d/%m%y %H:%M:%S')

        super(Logger, self).__init__(name, logging.CRITICAL)

        log_handler = logging.StreamHandler(kwargs['fid'])
        log_fmt = logging.Formatter(fmt=kwargs['fmt'], datefmt=kwargs['datefmt'])
        log_handler.setFormatter(log_fmt)
        self.addHandler(log_handler)
        self.setLevel(kwargs['level'])

    def setLevel(self, level):
        return super(Logger, self).setLevel(self._levels[level])

    def trace(self):
        def apply(callback):
            def _wrapper(*args, **kwargs):
                self.debug("<- START TRACE ->")
                self.debug(callback)
                self.debug(args)
                self.debug(kwargs)
                data = callback(*args, **kwargs)
                self.info(data)
                self.debug("<- END TRACE ->")
                return data
            return _wrapper
        return apply

                
