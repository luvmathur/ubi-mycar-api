import pytest
from stslib.database import Session, connect, models


@pytest.fixture
def engine():
    return connect()

@pytest.yield_fixture
def session(engine):
    with Session(engine) as _session:
        yield _session

def test_connect(engine):
    row = engine.execute('SELECT 1;').first()
    assert 1 == int(row[0])

def test_user(session):
    user = session.getUser(userID='itest')
    assert user is not None

