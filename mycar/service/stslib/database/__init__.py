from sqlalchemy import create_engine
from sqlalchemy.sql import exists
from session import Session
import models
import errors

__version__ = '0.1 Alpha'

## Put the models here.
__all__ = ['connect', 'Session', 'exists', 'models','errors']

SCHEMA_FORMAT = "mysql://{user}:{password}@{host}:{port}/{database}"

def connect(**kwargs):
    """Create database engine that can be used to interact with the STS Databa

    Keyword Arguments:
        host - The server hosting the database
        user - The mysql user used to connect to the database
        password - The mysql user's password used to connect to the database
        database - The mysql database schema that contains the OpenGTS database (default gts)
        
    Returns:
        A sqlalchemy engine that can be used to create sessions.
    """    
    kwargs.setdefault('user', 'gts')
    kwargs.setdefault('password', 'opengts')
    kwargs.setdefault('host', 'localhost')
    kwargs.setdefault('port', 3306)
    kwargs.setdefault('database', 'gts')
    kwargs.setdefault('pool_size', 10)
    kwargs.setdefault('pool_recycle', 7200)

    engine = create_engine(SCHEMA_FORMAT.format(**kwargs))
    models.Base.prepare(engine)

    return engine

