from sqlalchemy import Column, ForeignKey, Table, ForeignKeyConstraint
from sqlalchemy.orm import relationship, backref
from sqlalchemy.dialects.mysql import *
from sqlalchemy.ext.declarative import declarative_base, DeferredReflection
from sqlalchemy.ext.associationproxy import association_proxy
from datetime import datetime, date

Base = declarative_base(cls=DeferredReflection)

"""We have to define primary keys and relationships manualy, Everything else is auto populated"""

class AdpDealerMap(Base):
    __tablename__ = 'ADP_DealerMap'
    accountID = Column(VARCHAR(32), primary_key=True)
    companyID = Column(INTEGER(), primary_key=True)

class AdpInventoryMap(Base):
    __tablename__ = 'ADP_InventoryMap'
    accountID = Column(VARCHAR(32), primary_key=True)
    companyID = Column(INTEGER(11), primary_key=True)
    inventoryAccountID = Column(INTEGER(11), primary_key=True)
    inventoryType = Column(VARCHAR(50), primary_key=True)
    groupID = Column(VARCHAR(32), primary_key=True)

class Inventory(Base):
    __tablename__ = 'Inventory'
    vehicleID = Column(VARCHAR(32), primary_key=True)

class obdiicodes(Base):
    __tablename__ = 'obdiicodes'
    code = Column(VARCHAR(8), primary_key=True)
    make = Column(VARCHAR(15), primary_key=True)
    model = Column(VARCHAR(15), primary_key=True)
    priority = Column(VARCHAR(15), primary_key=True)

class Account(Base):
    __tablename__ = 'Account'

class User(Base):
    __tablename__ = 'User'

class GroupList(Base):
    __tablename__ = 'GroupList'

class AccountString(Base):
    __tablename__ = 'AccountString'

class Antx(Base):
    __tablename__ = 'Antx'
    accountID = Column(VARCHAR(32), primary_key=True);
    deviceID = Column(VARCHAR(32), primary_key=True);
    timestamp = Column(INTEGER(), primary_key=True);
    fieldID = Column(INTEGER(), primary_key=True);
    channelID = Column(INTEGER(), primary_key=True);

class Device(Base):
    __tablename__ = 'Device'
    accountID = Column(VARCHAR(32), primary_key=True)
    deviceID = Column(VARCHAR(32), primary_key=True)
    agreements = relationship("Agreements", backref="device", foreign_keys="[Agreements.accountID, Agreements.deviceID]")

    @property
    def odometerKM(self):
        _ret = 0
        if self.lastOdometerKM:
            _ret += float(self.lastOdometerKM)
        if self.odometerOffsetKM:
            _ret += float(self.odometerOffsetKM)
        return _ret

    @property
    def vehicleAge(self):
        _install = self.installTime
        if _install and float(_install) > 0:
            return (datetime.utcnow() - datetime.utcfromtimestamp(float(_install))).days
        return 0

    @property
    def lastFuelPercent(self):
        if self.lastFuelLevel:
            if self.lastFuelLevel > 1:
                return 1.00
            else:
                return self.lastFuelLevel
        return 0

    @property
    def serviceIntervalKM0(self):
        if self.maintIntervalKM0:
            return float(self.maintIntervalKM0)
        return self.odometerKM

    @property
    def serviceIntervalKM1(self):
        if self.maintIntervalKM1:
            return float(self.maintIntervalKM1)
        return self.odometerKM

    @property
    def serviceOdometerKM0(self):
        if self.maintOdometerKM0 is not None:
            return float(self.maintOdometerKM0)
        return self.odometerKM

    @property
    def serviceOdometerKM1(self):
        if self.maintOdometerKM1 is not None:
            return float(self.maintOdometerKM1)
        return self.odometerKM

    @property
    def nextServiceDetails(self):
        _nextService0 = self.serviceIntervalKM0 + self.serviceOdometerKM0
        _nextService1 = self.serviceIntervalKM1 + self.serviceOdometerKM1
        _remaining0 = _nextService0 - self.odometerKM
        _remaining1 = _nextService1 - self.odometerKM
        if _remaining0 == _remaining1:
            return { 
                'index' : -1,
                'intervalKM' : 0,
                'remainingKM' : 0,
                'serviceOdometerKM' : self.odometerKM,
                'percent' : 1.0
            }

        elif _remaining0 < _remaining1:
            ## Interval 0
            _n = self.serviceIntervalKM0 - _remaining0
            _p = 1.0
            if self.serviceIntervalKM0 > 0:
                _p = _n / self.serviceIntervalKM0

            return { 
                'index' : 0,
                'intervalKM' : self.serviceIntervalKM0,
                'remainingKM' : _remaining0,
                'serviceOdometerKM' : _nextService0,
                'percent' : _p,
            }

        ## Interval 1
        _n = self.serviceIntervalKM1 - _remaining1
        _p = 1.0
        if self.serviceIntervalKM1 > 0:
            _p = _n / self.serviceIntervalKM1

        return { 
            'index' : 1,
            'intervalKM':self.serviceIntervalKM1,
            'remainingKM':_remaining1,
            'serviceOdometerKM':_nextService1,
            'percent':_p,
        }

class AgreementType(Base):
    __tablename__ = 'AgreementType'
    ID = Column(INTEGER(), primary_key=True)
    Name = Column(VARCHAR(200))

class Agreements(Base):
    __tablename__ = 'Agreements'
    accountID = Column(VARCHAR(32))
    deviceID = Column(VARCHAR(32))
    ID = Column(VARCHAR(36), primary_key=True)
    TypeID = Column(INTEGER(), ForeignKey("AgreementType.ID"))
    agreementType = relationship("AgreementType")
    __table_args__ = (ForeignKeyConstraint([accountID, deviceID], [Device.accountID, Device.deviceID]), {})

    @property
    def agreementTypeName(self):
        """The type of agreement"""
        return self.agreementType.Name

    @property
    def agreementKM(self):
        """The agreement km agreed on at the sale of the car"""
        return float(self.DistanceKM or 0)

    @property
    def startOdometerKM(self):
        """The odometer value when the agreement was started"""
        return float(self.StartOdometerKM or 0)

    @property
    def relativeOdometerKM(self):
        """Difference between current device odometer and this agreement's starting odometer
           i.e. How many KM have been driven since this agreement started?
        """
        return float(max(0, self.device.odometerKM - self.startOdometerKM))

    @property
    def remainingKM(self):
        """The km left in the agreement"""
        distKM = float(self.DistanceKM) or 0
        return float(max(0, distKM - self.relativeOdometerKM))

    @property
    def remainingKMPercent(self):
        """The percent of remaining KM for the agreement"""
        if self.agreementKM > 0:
            return self.remainingKM / self.agreementKM
        return 0.00

    @property
    def agreementDays(self):
        """The total number of days in the agreement, inclusive of the first and last days
           e.g. Start 2015/1/1, End 2015/1/31 = 31 days
        """
        if self.StartDate and self.EndDate and self.EndDate >= self.StartDate:
            return (self.EndDate - self.StartDate).days + 1
        else:
            return 0.0

    @property
    def remainingDays(self):
        """The number of days remaining for the agreement, inclusive of the first and last days
           e.g. Today 2015/1/30, End 2015/1/31 = 2 days

            warning: uses date.today() which is local to the server, depending on the user's
            timezone, this could report a day less than is true for the user for a few hours.

            returns a non-zero value only if EndDate is non-null and EndDate >= today"""
        if self.EndDate:
            diff = self.EndDate - date.today()
            if diff.days >= 0:
                return diff.days + 1

        return 0

    @property
    def remainingDaysPercent(self):
        if self.agreementDays > 0:
            # casting is required here to force float division
            #TODO remove cast once we're ready to adopt 3.0 division compatibility
            return self.remainingDays / float(self.agreementDays)

class DeviceUI(Base):
    __tablename__ = 'DeviceUI'

class DeviceGroup(Base):
    __tablename__ = 'DeviceGroup'

class DeviceList(Base):
    __tablename__ = 'DeviceList'

class Rule(Base):
    __tablename__ = 'Rule'

class Diagnostic(Base):
    __tablename__ = 'Diagnostic'

class Driver(Base):
    __tablename__ = 'Driver'

class Entity(Base):
    __tablename__ = 'Entity'

class EventData(Base):
    __tablename__ = 'EventData'

    @property
    def gps_timestamp(self):
        return self.timestamp - (self.gpsAge or 0)


class EventTemplate(Base):
    __tablename__ = 'EventTemplate'

class FuelRegister(Base):
    __tablename__ = 'FuelRegister'

class GeoCorridor(Base):
    __tablename__ = 'GeoCorridor'

class GeoCorridorList(Base):
    __tablename__ = 'GeoCorridorList'

class Geozone(Base):
    __tablename__ = 'Geozone'

class GeozoneRules(Base):
    __tablename__ = 'GeozoneRules'

class NotifyQueue(Base):
    __tablename__ = 'NotifyQueue'

class PendingCommands(Base):
    __tablename__ = 'PendingCommands'

class PendingPacket(Base):
    __tablename__ = 'PendingPacket'

class Property(Base):
    __tablename__ = 'Property'

class ReportJob(Base):
    __tablename__ = 'ReportJob'

class Resource(Base):
    __tablename__ = 'Resource'

class Role(Base):
    __tablename__ = 'Role'

class RoleAcl(Base):
    __tablename__ = 'RoleAcl'

class RuleList(Base):
    __tablename__ = 'RuleList'

class RuleTrigger(Base):
    __tablename__ = 'RuleTrigger'

class SessionStats(Base):
    __tablename__ = 'SessionStats'

class StatusCode(Base):
    __tablename__ = 'StatusCode'

class SystemAudit(Base):
    __tablename__ = 'SystemAudit'

class SystemProps(Base):
    __tablename__ = 'SystemProps'

class Transport(Base):
    __tablename__ = 'Transport'

class UnassignedDevices(Base):
    __tablename__ = 'UnassignedDevices'

class UniqueXID(Base):
    __tablename__ = 'UniqueXID'

class UserAcl(Base):
    __tablename__ = 'UserAcl'

class vascontract(Base):
    __tablename__ = 'vascontract'

class vascontractdriver(Base):
    __tablename__ = 'vascontractdriver'

class vascontractvehicle(Base):
    __tablename__ = 'vascontractvehicle'

class DevicePublic(Base):
    __tablename__ = 'DevicePublic'

class AggregateEvent_Daily(Base):
    __tablename__ = "EventAggregate_Daily"

class AggregateEvent_Weekly(Base):
    __tablename__ = "EventAggregate_Weekly"

class AggregateEvent_Monthly(Base):
    __tablename__ = "EventAggregate_Monthly"

class TripDetail(Base):
    __tablename__ = "TripDetail"
    ID = Column(VARCHAR(36), primary_key=True)
    details = relationship("TripDetailEvents", order_by="TripDetailEvents.timestamp")

class TripDetailEvents(Base):
    __tablename__ = "TripDetailEvents"
    ID = Column(VARCHAR(36), primary_key=True)
    tripID = Column(VARCHAR(36), ForeignKey("TripDetail.ID"))


class TripActivityEvents(Base):
    __tablename__ = "TripActivityEvents"
    accountID = Column(VARCHAR(32), primary_key=True)
    deviceID = Column(VARCHAR(32), primary_key=True)
    timestamp = Column(INTEGER(), primary_key=True)
    statusCode = Column(INTEGER(), primary_key=True)


class EconomyActivityEvents(Base):
    __tablename__ = "EconomyActivityEvents"
    accountID = Column(VARCHAR(32), primary_key=True)
    deviceID = Column(VARCHAR(32), primary_key=True)
    timestamp = Column(INTEGER(), primary_key=True)
    statusCode = Column(INTEGER(), primary_key=True)

class PendingInstallation(Base):
    __tablename__ = "PendingInstallation"

class VehicleDetails(Base):
    __tablename__ = "VehicleDetails"

class DeviceUITemplate(Base):
    __tablename__ = "DeviceUITemplate"

"""
class PushNotificationRegID(Base):
    __tablename__ = "PushNotificationRegID"
"""



