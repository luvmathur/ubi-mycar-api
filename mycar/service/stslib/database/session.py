
from sqlalchemy.orm.session import Session as _Session
import models

from sqlalchemy import exc
from sqlalchemy import event
from sqlalchemy.pool import Pool

@event.listens_for(Pool, "checkout")
def ping_connection(dbapi_connection, connection_record, connection_proxy):
    cursor = dbapi_connection.cursor()
    try:
        cursor.execute("SELECT 1")
    except:
        # optional - dispose the whole pool
        # instead of invalidating one at a time
        # connection_proxy._pool.dispose()

        # raise DisconnectionError - pool will try
        # connecting again up to three times before raising.
        raise exc.DisconnectionError()
    cursor.close()

class Session(_Session):
    """Database Session used to interact with the STS Database"""

    def dispose(self):
        """Close database connection and dispose the database engine"""
        self.close()

    def __enter__(self):
        return self

    def __exit__(self, a,b,c):
        self.dispose()
        return True

    def getDevice(self, **kwargs):
        return self.query(models.Device).filter_by(**kwargs).first()

    def getUser(self, **kwargs):
        return self.query(models.User).filter_by(**kwargs).first()

    def getAccount(self, accountid):
        return self.query(models.Account)\
            .filter(models.Account.accountID==accountid)\
            .first()

    def getGroupsForUser(self, user):
        return self.query(models.GroupList)\
            .filter(models.GroupList.accountID==user.accountID)\
            .filter((models.GroupList.userID==user.userID) | (user.roleID=='!admin'))

    def getGroupsForUserByUserID(self, userid):
        user = self.getUser(userID=userid)
        return self.getGroupsForUser(user)

    def getDevicesInGroups(self, groups, accountid):
        return self.query(models.Device)\
            .join(models.DeviceList,\
                (models.Device.accountID == models.DeviceList.accountID) &\
                (models.Device.deviceID == models.DeviceList.deviceID))\
            .filter(models.Device.accountID == accountid)\
            .filter(models.DeviceList.groupID.in_(groups))\
            .distinct()

    
    def getGeozoneRules(self, groups, deviceid, accountid):
        """Get the Geozone rules for a specific accountID and deviceID

        NOTE: myCar devices have their groupID's set to the transferring Dealer's userID,
        thus this query will be empty when pulling Geozones made with Carmatics devices.
        """
        return self.query(models.Geozone, models.Rule)\
            .outerjoin(models.GeozoneRules, models.Geozone.geozoneID == models.GeozoneRules.geozoneID)\
            .outerjoin(models.Rule, models.GeozoneRules.ruleID == models.Rule.ruleID)\
            .outerjoin(models.RuleList, models.Rule.ruleID == models.RuleList.ruleID)\
            .filter(models.Geozone.accountID == accountid)\
            .filter(models.Geozone.groupID.in_(groups))\
            .filter(models.Geozone.zonePurposeID == deviceid)

