import pytest
from webtest import TestApp
from report import service

@pytest.fixture
def app():
    return TestApp(service)

