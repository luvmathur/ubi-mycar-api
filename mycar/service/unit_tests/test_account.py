import pytest
from account import service as WebService
from fixtures import webservice
from time import time

from stslib.database import Session, models, connect

OK = '200 OK'
ADMIN_UNAME = 'ian@skytrace.com'
ADMIN_PASSW = 'aee209ddbbddef27631c8ad0e1bb5521667ea6e0f12c2dabb7e3cb7b9cd8e174'

def util_timestamp():
    return int(time())

@webservice(WebService, ADMIN_UNAME, ADMIN_PASSW)
def test_1(service, **kw):
    """ TEST 1: Account Settings

    Verify we can retrieve settings for this account and cache the details.
    change the display name to an arbitrary vaule and verify the response.
    Then set it back to the initial state and verify the response.
    """
    new_display_name = str(util_timestamp())

    resp = service.get('/settings')
    assert resp.status == OK
    assert resp.json['accountID' ] == ADMIN_UNAME

    old_display_name = resp.json['displayName']
    checksum = resp.json['checksum']

    resp = service.put_json('/settings', params={ 'displayName' : new_display_name })
    assert resp.status == OK
    assert resp.json['displayName'] == new_display_name
    assert resp.json['checksum'] != checksum

    resp = service.get('/settings')
    assert resp.status == OK
    assert resp.json['displayName'] == new_display_name
    assert resp.json['checksum'] != checksum

    resp = service.put_json('/settings', params={ 'displayName' : old_display_name })
    assert resp.status == OK
    assert resp.json['displayName'] == old_display_name
    assert resp.json['checksum'] == checksum


@webservice(WebService, ADMIN_UNAME, ADMIN_PASSW)
def test_2(service, **kw):
    """TEST 2: User Settings

    Verify we can retireve a existing users data and cache the details.
    change the display name to an arbitrary value and verify the response.
    then set it back to the initial state and verify the response.
    """
    new_display_name = str(util_timestamp())
    resp = service.get('/user')
    assert resp.status == OK
    assert resp.json['userID' ] == ADMIN_UNAME

    old_display_name = resp.json['displayName']
    checksum = resp.json['checksum']

    resp = service.put_json('/user', params={ 'displayName' : new_display_name })
    assert resp.status == OK
    assert resp.json['displayName'] == new_display_name
    assert resp.json['checksum'] != checksum

    resp = service.put_json('/user', params={ 'displayName' : old_display_name })
    assert resp.status == OK
    assert resp.json['displayName'] == old_display_name
    assert resp.json['checksum'] == checksum


@webservice(WebService, ADMIN_UNAME, ADMIN_PASSW)
def test_3(service, **kw):
    """TEST 3: User Managment

    Create a new user with arbitrary settings.
    activate the account and verify response.
    pull account and verify new user exists and the value's are correct.
    modify the users groups and set display name to some arbitrary vaule
    verify we can log in as the user and verify the state
    delete the user account
    """
    new_user_password = 'abc123'
    new_user = {
        ## Required Fields
        'userID' : 'en0@mail.com',
        'contactName' : 'richard laird',
        'contactPhone' : '555-555-1234',
        'addressLine1' : '123 maple ave.',
        'addressCity' : 'somewhere',
        'addressState' : 'ST',
        'addressPostalCode' : '00000',
        'addressCountry' : 'USA',
    
        ## Optional Fields
        'displayName' : 'laird, richard',
    }

    ## Pull current settings
    resp = service.get('/settings')
    settings_checksum = resp.json['checksum']

    ## Add User
    resp = service.post_json('/user', params=new_user)
    assert resp.status == OK
    activation_key = getActivationEmail(new_user['userID'])
    assert activation_key is not None

    ## user is not active yet so settings should not show any diffrence
    resp = service.get('/settings')
    assert resp.json['checksum'] == settings_checksum

    ## Activate Account
    activation_json = {
        'activation_key' : activation_key,
        'password' : new_user_password,
        'email' : new_user['userID'],
    }
    resp = service.post_json('/user/activate', params=activation_json)
    assert resp.status == OK

    ## user is active so settings should show diffrence in checksum
    resp = service.get('/settings')
    assert resp.json['checksum'] != settings_checksum

    ## verify account
    resp = service.get('/user/%s' % new_user['userID'])
    print(resp.json)
    assert resp.status == OK
    assert resp.json['displayName'] == new_user['displayName']
    assert new_user['userID'] in resp.json['groups']
    assert ADMIN_UNAME not in resp.json['groups']
    new_user_checksum = resp.json['checksum']

    ## Modify the users account
    new_display_name = str(util_timestamp())
    resp = service.put_json('/user/%s' % new_user['userID'], params={ 'displayName' : new_display_name, 'groups' : [ ADMIN_UNAME ] })
    assert resp.status == OK
    assert resp.json['checksum'] != new_user_checksum
    assert resp.json['displayName'] == new_display_name
    assert new_user['userID'] in resp.json['groups']
    assert ADMIN_UNAME in resp.json['groups']

    ## Verify we can log in as new user
    @webservice(WebService, new_user['userID'], new_user_password)
    def _login_as_user(**kwargs):
        _service = kwargs['service']
        _resp = _service.get('/user')
        assert resp.status == OK
    ##_login_as_user(mokeypatch=kw['mp'])
        
    ## Delete Account
    resp = service.delete('/user/%s' % new_user['userID'])
    assert resp.status == '200 OK'

    ## Verify checksum is back where it was and all signs of the user are gone
    resp = service.get('/settings')
    assert resp.json['checksum'] == settings_checksum


def getActivationEmail(userid):
    engine = connect()
    with Session(engine) as db:
        key = db.query(models.User.activationKey)\
            .filter(models.User.userID==userid).scalar()
        print("HERE IS THE ACTIVATION KEY: ", key)
        return key

