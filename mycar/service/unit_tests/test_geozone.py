import pytest
from geozone import service as WebService
from fixtures import webservice

OK = "200 OK"

ADMIN_PASSWD = 'aee209ddbbddef27631c8ad0e1bb5521667ea6e0f12c2dabb7e3cb7b9cd8e174'

@webservice(WebService, 'ian@skytrace.com', ADMIN_PASSWD)
def test_1(service, **kw):
    """ TEST 1
    Create a new geozone
    add 2 rules to geozone
    delete geozone
    """
    resp = service.get('/devek_000000002')
    assert resp.status == OK
    checksum = resp.json['checksum']

    new_geozone = {
        'displayName': 'TEST ZONE',
        'latitude1': 43.631844,
        'longitude1': -116.382728,
        'radius': 15,
    }

    resp = service.post_json('/devek_000000002', params=new_geozone)
    assert resp.status == OK
    new_geozone = resp.json

    resp = service.get('/devek_000000002')
    assert resp.status == OK
    assert checksum != resp.json['checksum']

    new_geozone['rules'].append({
                'notifyEmail': 'ian@skytrace.com',
                'emailSubject': 'First New Geozone!',
                'selector': '$ARRIVE("%s")' % new_geozone['geozoneID'],
                'actionMask': 65799,
                'emailText': 'This is a email text',
                'isActive': False
            })

    new_geozone['rules'].append({
                'notifyEmail': 'ian@skytrace.com',
                'emailSubject': 'Second New Geozone!',
                'selector': '$INZONE("%s")' % new_geozone['geozoneID'],
                'actionMask': 65799,
                'emailText': 'This is a email text',
                'isActive': False
            })

    resp = service.put_json('/devek_000000002/%s' % new_geozone['geozoneID'], params=new_geozone)
    assert resp.status == OK

    resp = service.delete('/devek_000000002/%s' % new_geozone['geozoneID'])
    assert resp.status == OK

    resp = service.get('/devek_000000002')
    assert resp.status == OK
    checksum = resp.json['checksum']


@webservice(WebService, 'ian@skytrace.com', ADMIN_PASSWD)
def test_2(service, **kw):
    """ TEST 2
    Create a new valet zone
    delete geozone
    """
    resp = service.get('/devek_000000002')
    assert resp.status == OK
    checksum = resp.json['checksum']

    new_geozone = {
        'displayName': 'RAV4 - Valet Parking',
        'latitude1': 43.631844,
        'longitude1': -116.382728,
        'radius': 100,
    }

    resp = service.post_json('/devek_000000002?valet=true', params=new_geozone)
    assert resp.status == OK
    new_geozone = resp.json

    resp = service.get('/devek_000000002')
    assert resp.status == OK
    assert checksum != resp.json['checksum']

    resp = service.delete('/devek_000000002/%s?valet=true' % new_geozone['geozoneID'])
    assert resp.status == OK

    resp = service.get('/devek_000000002')
    assert resp.status == OK
    checksum = resp.json['checksum']
