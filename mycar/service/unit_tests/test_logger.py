#!/bin/env python
import pytest
import StringIO
from core import Logger


@pytest.fixture
def fid():
    return StringIO.StringIO()


def test_debug(fid):
    log = Logger('pytest', level='DEBUG', fid=fid)

    log.debug("__debug__")
    log.info("__info__")
    log.warning("__warning__")
    log.error("__error__")
    log.critical("__critical__")
    
    assert '__debug__' in fid.getvalue()
    assert '__info__' in fid.getvalue()
    assert '__warning__' in fid.getvalue()
    assert '__error__' in fid.getvalue()
    assert '__critical__' in fid.getvalue()
    
    fid.close()


def test_info(fid):
    log = Logger('pytest', level='DEBUG', fid=fid)

    log.debug("__debug__")
    log.info("__info__")
    log.warning("__warning__")
    log.error("__error__")
    log.critical("__critical__")
    
    assert '__info__' in fid.getvalue()
    assert '__warning__' in fid.getvalue()
    assert '__error__' in fid.getvalue()
    assert '__critical__' in fid.getvalue()
    
    fid.close()


def test_warning(fid):
    log = Logger('pytest', level='DEBUG', fid=fid)

    log.debug("__debug__")
    log.info("__info__")
    log.warning("__warning__")
    log.error("__error__")
    log.critical("__critical__")
    
    assert '__warning__' in fid.getvalue()
    assert '__error__' in fid.getvalue()
    assert '__critical__' in fid.getvalue()
    
    fid.close()


def test_error(fid):
    log = Logger('pytest', level='DEBUG', fid=fid)

    log.debug("__debug__")
    log.info("__info__")
    log.warning("__warning__")
    log.error("__error__")
    log.critical("__critical__")
    
    assert '__error__' in fid.getvalue()
    assert '__critical__' in fid.getvalue()
    
    fid.close()


def test_critical(fid):
    log = Logger('pytest', level='DEBUG', fid=fid)

    log.debug("__debug__")
    log.info("__info__")
    log.warning("__warning__")
    log.error("__error__")
    log.critical("__critical__")
    
    assert '__critical__' in fid.getvalue()
    
    fid.close()

