import pytest
from webtest import TestApp
from hmac import new as HMAC
from base64 import b64encode
from hashlib import sha1
from time import time


def webservice(app, user, passwd):
    def apply(callback):
        def _wrapper(monkeypatch, *args, **kwargs):
            _key = authkey(user, passwd)
            monkeypatch.setattr(app, '_getAuthKey', lambda: _key)
            kwargs['service'] = TestApp(app)
            kwargs['mp'] = monkeypatch
            return callback(*args, **kwargs)
        return _wrapper
    return apply


def authkey(user, passwd):
    utcTS = int(time())
    hash = b64encode(HMAC(''.join([str(utcTS),passwd]), user, sha1).digest())
    return { 'userID' : user, 'utcTS' : utcTS, 'hash' : hash, }

