import pytest
from device import service as WebService
from fixtures import webservice

OK = "200 OK"

ADMIN_PASSWD = 'aee209ddbbddef27631c8ad0e1bb5521667ea6e0f12c2dabb7e3cb7b9cd8e174'

@webservice(WebService, 'ian@skytrace.com', ADMIN_PASSWD)
def test_1(service, **kw):
    """ Test 1: Pull list of all devices and verify.

    Verify that "All Devices" functions correctly and accuratly reflects
    user's set of cars
    """
    resp = service.get('/')
    assert resp.status == OK


@webservice(WebService, 'ian@skytrace.com', ADMIN_PASSWD)
def test_2(service, **kw):
    """ Test 2: Pull device details for prefered device
    """
    resp = service.get('/detail')
    assert resp.status == OK


@webservice(WebService, 'ian@skytrace.com', ADMIN_PASSWD)
def testi_3(service, **kw):
    """ Test 3: Pull location for preferred device
    """
    resp = service.get('/location')
    assert resp.status == OK


@webservice(WebService, 'ian@skytrace.com', ADMIN_PASSWD)
def test_4(service, **kw):
    """ test 4: Alt Device Detail

    Pull device details for alternate device
    verify the changes where saved
    reset device and verify.
    """
    resp = service.get('/detail/devel_000000001?update=true')
    assert resp.status == OK
    cksum = resp.json['checksum']

    resp = service.get('/detail')
    assert resp.status == OK
    assert cksum == resp.json['checksum']

    resp = service.get('/detail/devek_000000002?update=true')
    assert resp.status == OK
    assert cksum != resp.json['checksum']


@webservice(WebService, 'ian@skytrace.com', ADMIN_PASSWD)
def test_5(service, **kw):
    """ Test 5: Alt device location

    pull location for alt device
    verify it does not override preferred
    pull location for alt and save
    verify chagnes where saved and reset al
    device.
    """
    resp = service.get('/location/devel_000000001?update=true')
    assert resp.status == OK
    cksum = resp.json['checksum']
    
    resp = service.get('/location')
    assert resp.status == OK
    assert cksum == resp.json['checksum']

    resp = service.get('/location/devek_000000002?update=true')
    assert resp.status == OK
    assert cksum != resp.json['checksum']



