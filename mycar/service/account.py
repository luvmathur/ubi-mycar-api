from core import WebService, request, abort
from core import utils, hooks, get_resource
from stslib.database import models, errors
from hashlib import sha1
from quik import Template
from vehicle import is_valid
import re

service = WebService('account', 'app.config')

@service.priv_route('/v<version>/account/settings', method=['GET','OPTIONS'])
@hooks.inrole("!admin")
@hooks.checksum(sha1)
def get_settings_admin(account, user, groups, db, version):
    """Retrieve account details. 

    ROUTE: /settings
    METHOD: GET

    Only admins can pull this data. Users are included for reference and by proxy
    the groups can be derived from the user list as the groupids match the userids
    and each user has a group and no other groups exist.
    """
    if request.method == "OPTIONS": return

    service.logger.debug("{0} GET: /settings".format(version))
    return send_settings(user, db)


@service.priv_route('/v<version>/account/settings', method=['PUT','OPTIONS'])
@hooks.inrole("!admin")
@hooks.checksum(sha1)
def put_settings_admin(account, user, groups, db, version):
    """Update account details

    ROUTE: /settings
    METHOD: PUT

    Only admin can put data to this function. Only specific fields can be updated
    and everything else is ignored. Accounts are accessed via current users ID
    so any attempt to circumvent the lookup will not work.
    """
    if request.method == "OPTIONS": return

    service.logger.debug("{0} PUT: /settings".format(version))
    service.logger.debug(request.json)

    safe = utils.pack(request.json, [
        "contactName",
        "displayName",
        "contactPhone",
        "contactEmail",
        "notifyEmail",
        "speedUnits",
        "distanceUnits",
        "volumeUnits",
        "pressureUnits",
        "economyUnits",
        "temperatureUnits",
    ], normalize=False)
    
    db.query(models.Account)\
        .filter(models.Account.accountID==user.accountID)\
        .update(safe)

    db.commit()
    return send_settings(user, db)


def send_settings(user, db):
    """Return the users account settings.

    This is broke out to avoid errors from inrole and to allow
    access to other functions that might need this data.

    Arguments:
        user - the models.User for the current user
        db - the database context
    
    Returns:
        Packed json object of account data
    """
    account = db.getAccount(user.accountID)
    val = utils.pack(account, [
        "accountID",
        "displayName",
        "contactName",
        "contactPhone",
        "contactEmail",
        "notifyEmail",
        "speedUnits",
        "distanceUnits",
        "volumeUnits",
        "pressureUnits",
        "economyUnits",
        "temperatureUnits",
    ])

    val["users"] = [dict(
        userID=x.userID,
        displayName=x.displayName
    ) for x in db.query(models.User)\
        .filter(models.User.accountID==user.accountID)\
        .filter(models.User.isActive==True)]

    service.logger.debug('SEND_SETTINGS')
    service.logger.debug(val)
    return val


@service.priv_route('/v<version>/account/user', method=['GET','OPTIONS'])
@hooks.checksum(sha1)
def get_user(account, user, groups, db, version):
    """Retrieve basic user settings
    
    METHOD: GET
    ROUTE: /user

    This method can be ran by any valid user.
    it returns the current users settings.
    """
    if request.method == "OPTIONS": return;

    service.logger.debug("{0} GET: /user".format(version))

    rules = db.query(models.Rule)\
        .filter(models.Rule.accountID == user.accountID)\
        .filter(models.Rule.ruleTag == 'iCAR')

    return send_user(user, groups, account, rules)


@service.priv_route('/v<version>/account/user', method=['PUT','OPTIONS'])
@hooks.checksum(sha1)
def put_user(account, user, groups, db, version):
    """save the provided resource to the active users record.

    ROUTE: /user
    METHOD: PUT

    Any user can run this function and it only allows the saving of the current
    user's data. A subset of the data is returned and group list is attached to 
    the resource details.
    """
    if request.method == "OPTIONS": return;

    service.logger.debug("{0} PUT /user".format(version))
    service.logger.debug(request.json)

    safe = utils.pack(request.json, [
        "contactName",
        "contactEmail",
        "contactPhone",
        "timeZone",
        "displayName",
        "userImage",
        "notifyEmail",
        "addressLine1",
        "addressLine2",
        "addressPostalCode",
        "addressState",
        "addressCity",
        "addressCountry",
    ], normalize=False)

    db.query(models.User)\
        .filter(models.User.userID == user.userID)\
        .filter(models.User.accountID == user.accountID)\
        .update(safe)

    for r in request.json['alerts']:
        db.query(models.Rule)\
            .filter(models.Rule.accountID == user.accountID)\
            .filter(models.Rule.ruleTag == 'iCAR')\
            .filter(models.Rule.ruleID == r['id'])\
            .update({ 'isActive' : (1 if r['enabled'] else 0) })

    db.commit()

    user = db.getUser(userID=user.userID)
    rules = db.query(models.Rule)\
        .filter(models.Rule.accountID == user.accountID)\
        .filter(models.Rule.ruleTag == 'iCAR')
    return send_user(user,groups, account, rules)


def send_user(user, groups, account, rules=None):
    """Format a normal user request to be sent down to the requester.
    
    this function is used to format the request for any user and is not
    limited to just the active user. Please use caution when calling this
    function that you do not expose the data to the incorrect party.

    Arguments:
        user    - The user object to format
        groups  - the list of groups the user is in.
    """


    val = utils.pack(user, [
        "userID",
        "roleID",
        "contactName",
        "contactEmail",
        "contactPhone",
        "timeZone",
        "displayName",
        "userImage",
        "notifyEmail",
        "addressLine1",
        "addressLine2",
        "addressPostalCode",
        "addressState",
        "addressCity",
        "addressCountry",
    ])

    val["accountType"] = account.accountType
    val['overspeedAlert'] = account.overspeedAlert
    val['geofencingAlert'] = account.geofencingAlert
    val['milAlert'] = account.milAlert
    val["groups"] = groups

    conv = utils.UnitConverter(account)

    val['units'] = { 
        "distance" : { 'symbol' : conv.distance_symbol, 'label' : conv.distance_label, },
        "volume" : { 'symbol' : conv.volume_symbol, 'label' : conv.volume_label, },
        "pressure" : { 'symbol' : conv.pressure_symbol, 'label' : conv.pressure_label, },
        "economy" : { 'symbol' : conv.economy_symbol, 'label' : conv.economy_label, },
        "temperature" : { 'symbol' : conv.temperature_symbol, 'label' : conv.temperature_label, },
    }

    if(rules):
        val['alerts'] = [ dict([ 
            ('name', rule.description), 
            ('id', rule.ruleID), 
            ('enabled', (rule.isActive == 1)) 
        ]) for rule in rules ]

    

    service.logger.debug('SEND_USER:')
    service.logger.debug(val)
    return val


@service.priv_route('/v<version>/account/user/<userid>', method=['GET','OPTIONS'])
@hooks.inrole("!admin")
@hooks.validateid(models.User.userID, 'userid')
@hooks.checksum(sha1)
def get_user_admin(userid, account, user, groups, db, version):
    """get the resource for the requested users record.

    ROUTE: /user/<userid>
    METHOD: GET

    Only !admin's can execute this function. It will retrieve a specific user by the
    id provided in the url. The user must be in there account. The data is returned
    in the same format as '/user'.
    """
    if request.method == "OPTIONS": return

    service.logger.debug("{0} GET: /user/{1}".format(version, userid))
    _user = db.getUser(userID=userid,accountID=user.accountID)
    _groups = [g.groupID for g in db.getGroupsForUser(_user)]
    return send_user(_user, _groups, account)


@service.priv_route('/v<version>/account/user/<userid>', method=['PUT','OPTIONS'])
@hooks.inrole("!admin")
@hooks.validateid(models.User.userID, 'userid')
@hooks.checksum(sha1)
def put_user_admin(userid, account, user, groups, db, version):
    """Update the users record that is specified in userid

    METHOD: PUT
    ROUTE: /user/<userid>

    This method is only accessible to !admin. All the normal details
    about the user can be updated as well as the role id.

    this is the only method that will set the roleID for a user.
    Limits around the roleID field are as follows.
    
    - No modifications can be made to roleID if the request came from
      the same user that is being modified.

    - No modification can be made to roleID if the modified user is
      the account holder - IE: the userid == accountid.


    This is the only method that allows you to add a user or remove
    a user from a group. Limits around this action are as follows.

    - Users that are admin or are becoming admin have no actions
      preformed on there group membership. They will be granted
      access because they are an admin and the effort is waisted.

    - Users cannot be removed from there user group, that is to say
      the user will never be removed when the userid == groupid.

    - Groups are validated against the DeviceGroup table and must 
      exist there for them to be added to the user.

    Additional limits are applied to lock all these actions to the 
    account of the requesting user only.
    """
    if request.method == "OPTIONS": return

    service.logger.debug("{0} PUT: /user/{1}".format(version, userid))
    service.logger.debug(request.json)

    safe_keys = [
        "contactName",
        "contactEmail",
        "contactPhone",
        "timeZone",
        "displayName",
        "userImage",
        "notifyEmail",
    ]

    ## It is not alowed to revoke admin from the account owner
    ## it is also not alowed to revoke admin from yourself.
    if userid != user.userID and userid != user.accountID:
        safe_keys.append('roleID')

    safe = utils.pack(request.json, safe_keys, normalize=False)
    db.query(models.User)\
        .filter(models.User.userID==userid)\
        .filter(models.User.accountID==user.accountID)\
        .update(safe)

    _isadmin = db.query(models.User.roleID=='!admin')\
        .filter(models.User.userID==userid)\
        .filter(models.User.accountID==user.accountID)\
        .first()

    service.logger.debug('Is this guy an admin? %s' % _isadmin[0])
    
    ## It doesn't matter what groups a admin is in.
    ## If the user is a admin then we dont want to waste  our time.

    ## This could be rebuilt to simplify things but lets wait till we are in a sustaining phase.
    if not _isadmin[0] and 'groups' in request.json:
        ## We need 3 things. The current groups, the requested groups, and the valid groups.
        current_groups = set(g.groupID for g in db.getGroupsForUserByUserID(userid))
        target_groups = set(request.json.get('groups', []))
        valid_groups = set([g.groupID for g in db.query(models.DeviceGroup.groupID)\
            .filter(models.DeviceGroup.accountID == user.accountID)\
            .filter(models.DeviceGroup.groupID != userid)])

        service.logger.debug("User is current in the following groups")
        service.logger.debug(current_groups)

        ## The current groups - the target groups will tell us what 
        ## we should remove the user from. Protect the users self group.
        service.logger.debug("Plan to remove user from...")
        for s in current_groups - target_groups:
            service.logger.debug(s)
            result = db.query(models.GroupList)\
                .filter(models.GroupList.accountID == user.accountID)\
                .filter(models.GroupList.userID == userid)\
                .filter(models.GroupList.groupID != userid)\
                .filter(models.GroupList.groupID == s)\
                .delete()
            service.logger.debug("Removed: %i records" % result)
                

        ## Filter requested groups to only valid ones and remove the
        ## groups the user is already in to avoid a pk exception.
        service.logger.debug("Plan to add user to...")
        for s in valid_groups.intersection(target_groups) - current_groups:
            service.logger.debug(s)
            new_membership = models.GroupList()
            new_membership.groupID = s
            new_membership.userID = userid
            new_membership.accountID = user.accountID
            new_membership.creationTime = str(utils.utc_timestamp())
            db.add(new_membership)

    db.commit()

    ## send the resource back
    _user = db.getUser(userID=userid,accountID=user.accountID)
    _groups = [g.groupID for g in db.getGroupsForUser(_user)]
    return send_user(_user, _groups, account)


@service.priv_route('/v<version>/account/user/<userid>', method=['DELETE','OPTIONS'])
@hooks.inrole('!admin')
@hooks.validateid(models.User.userID, 'userid')
@hooks.asStatusEntity()
def delete_user_admin(userid, account, user, groups, db, version):
    """Disable a user account and remove associations.

    METHOD: DELETE
    ROUTE: /user/<userid>

    This method can only be called by '!admin'. This method
    will not disable the active user and will not disable
    the account owner. 
    """
    if request.method == "OPTIONS": return

    service.logger.debug("{0} DELETE: /user/{1}".format(version, userid))
    if user.userID == userid or user.accountID == userid:
        abort(409, "Cannot delete account owner or active user.")

    ## Remove the user's group memberships
    db.query(models.GroupList)\
        .filter(models.GroupList.accountID == user.accountID)\
        .filter(models.GroupList.userID == userid)\
        .delete()

    ## Remove the device memeberships to the users group
    db.query(models.DeviceList)\
        .filter(models.DeviceList.accountID == user.accountID)\
        .filter(models.DeviceList.groupID == userid)\
        .delete()

    ## disassociate the geozone from ther user
    db.query(models.Geozone)\
        .filter(models.Geozone.accountID == user.accountID)\
        .filter(models.Geozone.groupID == userid)\
        .update({'groupID':''})

    ## Locate all rules owned by the user and disable them.
    ## also assign the rules to the account owner
    rules = db.query(models.Rule, models.RuleList)\
        .filter(models.RuleList.ruleID == models.Rule.ruleID)\
        .filter(models.Rule.accountID == user.accountID)\
        .filter(models.RuleList.accountID == user.accountID)\
        .filter(models.RuleList.groupID == userid)

    for row in rules:
        row.Rule.isActive = False
        row.RuleList.groupID = user.accountID

    ## Delete the users group
    db.query(models.DeviceGroup)\
        .filter(models.DeviceGroup.accountID == user.accountID)\
        .filter(models.DeviceGroup.groupID == userid)\
        .delete()

    key = str(utils.utc_timestamp())
    delete_key = utils.hmac(key, key)

    ## Disable the users accont
    db.query(models.User)\
        .filter(models.User.accountID == user.accountID)\
        .filter(models.User.userID == userid)\
        .update({ 
            'isActive' : False, 
            'userID' : delete_key, 
            'accountID' : '__REMOVED__',
            'password' : None,
            'notes' : 'User Deleted - Account: %s, User: %s' % (user.accountID, userid),
        })

    db.commit()

    return "Success: user removed.";


@service.priv_route('/v<version>/account/user', method=['POST','OPTIONS'])
@hooks.inrole('!admin')
@hooks.asStatusEntity()
def post_user_admin(account, user, groups, db, version):
    """Create user account

    METHOD: POST
    ROUTE: /user

    This method can be ran by !admin to create a user account. If the account
    already exists, or the name is duplicated a 409, conflict will be return.
    if not enough information is provided, a 400, bad request will be returned.
    """
    if request.method == "OPTIONS": return

    service.logger.debug("{0} POST: /user".format(version))
    utcts = str(utils.utc_timestamp())
    activation_key = utils.hmac(utcts, user.accountID)

    ## Required Fields
    if 'userID' not in request.json: abort(400, "Missing Field")
    if 'contactName' not in request.json: abort(400, "Missing Field")
    if 'contactPhone' not in request.json: abort(400, "Missing Field")
    if 'addressLine1' not in request.json: abort(400, "Missing Field")
    if 'addressCity' not in request.json: abort(400, "Missing Field")
    if 'addressState' not in request.json: abort(400, "Missing Field")
    if 'addressPostalCode' not in request.json: abort(400, "Missing Field")
    if 'addressCountry' not in request.json: abort(400, "Missing Field")
    
    ## Optional Fields
    request.json.setdefault('roleID', '')
    request.json.setdefault('timeZone', user.timeZone)
    request.json.setdefault('preferredDeviceID', '')
    request.json.setdefault('displayName', request.json['contactName'])

    ## Create new user
    new_user = models.User()

    ## SET Required Fields
    new_user.userID = request.json['userID']
    new_user.contactName = request.json['contactName']
    new_user.contactPhone = request.json['contactPhone']
    new_user.addressLine1 = request.json['addressLine1']
    new_user.addressCity = request.json['addressCity']
    new_user.addressState = request.json['addressState']
    new_user.addressPostalCode = request.json['addressPostalCode']
    new_user.addressCountry = request.json['addressCountry']

    ## SET Optional Fields
    new_user.roleID = request.json['roleID']
    new_user.timeZone = request.json['timeZone']
    new_user.preferredDeviceID = request.json['preferredDeviceID']
    new_user.displayName = request.json['displayName']

    ## SET static and generated fields
    new_user.accountID = user.accountID
    new_user.notifyEmail = request.json['userID']
    new_user.contactEmail = request.json['userID']
    new_user.description = 'Generated by %s' % service.service_url
    new_user.userType = 0
    new_user.gender = 0
    new_user.firstLoginPageID = 'menu.top'
    new_user.maxAccessLevel = 3
    new_user.passwdChangeTime = utcts
    new_user.passwdQueryTime = 0
    new_user.lastLoginTime = 0
    new_user.isActive = False ## < account is disabled untill the activation link is clicked by the user.
    new_user.notes = ''
    new_user.lastUpdateTime = utcts
    new_user.creationTime = utcts
    new_user.addressLine2 = None
    new_user.addressLine3 = None
    new_user.officeLocation = None
    new_user.userImage = None
    new_user.password = None

    ## SET the activation key
    new_user.activationKey = activation_key

    ## Create the users group
    user_group = models.DeviceGroup()
    user_group.accountID = user.accountID
    user_group.groupID = request.json['userID']
    user_group.lastUpdateTime = utcts
    user_group.creationTime = utcts

    ## Create the users membership
    group_membership = models.GroupList()
    group_membership.accountID = user.accountID
    group_membership.userID = request.json['userID']
    group_membership.groupID = request.json['userID']
    group_membership.creationTime = utcts

    try:
        db.add(new_user)
        db.add(user_group)
        db.add(group_membership)
        db.commit()
    except errors.IntegrityError:
        db.rollback()
        abort(409, 'Failed to create account.')
    
    service.send_activation(request.json['contactName'], request.json['userID'], activation_key)
    return "Success: User created, they will need to activate before you see them in the user's list";

@service.route('/v<version>/create/user/entry', method=['POST','OPTIONS'])
@service.db
@hooks.asStatusEntity()
def signin_newuser(db, version):
    """
    Initiate a new sign up request. Adds one entry in the PendingInstall table and
    send an activation link to the user which will redirect the user to dealmatics page to add
    Full name and password

    Args:
        db:
        version:

    Returns:
        Status (Success/fail)
    """
    if request.method == "OPTIONS": return

    service.logger.debug("{0} POST: /create/user/entry".format(version))

    if 'email' not in request.json: abort(400, "Missing Field")
    utcts = str(utils.utc_timestamp())
    activation_key = utils.hmac(utcts, request.json['email'])

    accnt = db.query(models.User)\
    .filter(models.User.userID == request.json['email'])\
    .first()

    if accnt : abort(404, "User already registered")

    pi_entry = db.query(models.PendingInstallation)\
    .filter(models.PendingInstallation.email == request.json['email'])\
    .first()

    if pi_entry:

        # Checking if the any user already initiated a mycar for this email from carmatics.
        if is_valid(pi_entry.VIN) and pi_entry.accountID != 'retail':
            return {"statusCode" : 404, "message" : "Thank you for signing up! The dealer is processing your account and it will be fully activated at your myCar installation appointment."}
        pi_entry.activationID = activation_key
    else:
        #Create a new Pending Installation entry
        pendingInstall = models.PendingInstallation()
        pendingInstall.accountID = "retail"
        pendingInstall.creationTime = utcts
        pendingInstall.email = request.json['email']
        pendingInstall.activationID = activation_key
        pendingInstall.VIN = utcts
        db.add(pendingInstall)

    try:
        db.commit()
    except Exception as ex:
        db.rollback()
        abort(409, 'Failed to create pending install entry')

    service.send_pending_activation(request.json['email'], activation_key)
    return "Success: Activation email sent"

@service.route('/v<version>/account/user/activate', method=['POST','OPTIONS'])
@service.db
@hooks.asStatusEntity()
def post_user_activate(db, version):
    """Activate a user account

    METHOD: POST
    ROUTE: /user/activate
    
    This method can be ran by anyone and will activate the account if
    the provided email matches the activation code. 

    It will also reset the password to the provided value
    """
    if request.method == "OPTIONS": return

    service.logger.debug("{0} POST: /user/activate".format(version))

    if 'activation_key' not in request.json:
        abort(400, 'Missing Details')
    if 'password' not in request.json:
        abort(400, 'Missing Details')
    if 'email' not in request.json:
        abort(400, 'Missing Details')

    user = db.getUser(userID=request.json['email'])
    if not user or user.activationKey != request.json['activation_key']:
        abort(400, "Bad email or invalid key")

    first_activation = user.password is None

    user.activationKey = None
    new_salt = utils.generate_new_salt()
    user.salt = new_salt
    user.password = utils.compute_salted_password(request.json['password'], new_salt)
    user.isActive = True
    
    if first_activation:
        account = db.getAccount(request.json['email'])
        if account:
            account.isActive = True

    # db.rollback()
    db.commit()
    return "Success: User is active"
    

@service.route('/v<version>/account/user/activate/<activation_key>', method=['GET','OPTIONS'])
def get_user_activate(version, activation_key):
    """Web Activation Page in case the user does not have access to the mobile application"""
    if request.method == "OPTIONS": return

    service.logger.debug("{0} GET: /user/activate/{1}".format(version, activation_key))
    mime_type, html = get_resource('user_activate.html')
    return Template(html).render({'key':activation_key})


@service.route('/v<version>/account/user/password/reset', method=['POST','OPTIONS'])
@service.db
@hooks.asStatusEntity()
def post_user_password_reset(db, version):
    """Send activation email and empty the password

    POST: /user/password/reset

    This method can be ran by anyone. It will send a activation to the email provided
    It will not empty the password and this action can be canceled if the request didn't
    originate from the user.
    """
    if request.method == "OPTIONS": return

    service.logger.debug("{0} POST: /user/password/reset".format(version))
    service.logger.debug("PARAMS: "+request.json.get('email'));
    #abort(404, request.json.get('email', '__noexist__'))
    user = db.query(models.User)\
        .filter(models.User.userID == request.json.get('email', '__noexist__'))\
        .first()

    if not user: abort(404, "Not Found")

    utcts = str(utils.utc_timestamp())
    activation_key = utils.hmac(utcts, user.accountID)
    user.activationKey = activation_key
    db.commit();

    service.send_forgot_password(user.contactName, user.userID, activation_key)
    return activation_key;


@service.priv_route('/v<version>/account/user/password/reset/<activation_key>', method=['GET','OPTIONS'])
@hooks.asStatusEntity()
def get_user_cancel_password_reset(db, user, version, activation_key, **kw):
    """Cancel the password reset request and disable the reactivation code.

    GET: /user/password/reset/<activation_key>

    This method can be ran by any authenticated user. the activation_key must
    be included in the url. This function will remove the activation key from
    the users record so the password cannot be reset.
    """
    if request.method == "OPTIONS": return

    service.logger.debug("{0} GET: /user/password/reset/{1}".format(version, activation_key))
    if user.activationKey ==  activation_key:
        user.activationKey = None
        db.commit()
    else:
        abort(400, "Not Applicable or incorrect key")

    return "Success: Password reset canceled";

@service.priv_route('/v<version>/account/user/password', method=['POST','OPTIONS'])
@hooks.asStatusEntity()
def post_user_change_password(db, user, version, **kw):
    """Change the users password to that specified in the post details
    
    POST: /user/password

    Any authenticated user can execute this method. it will change their password
    to the one provided.
    """
    if request.method == "OPTIONS": return

    service.logger.debug("{0} POST: /user/password".format(version))

    hashed_current = utils.compute_salted_password(
        request.json.get('current', '__noexist__'), user.salt)

    if user.password == hashed_current and 'new' in request.json:
        new_salt = utils.generate_new_salt()
        user.salt = new_salt
        user.password = utils.compute_salted_password(request.json['new'], new_salt)
        db.commit()
    else:
        abort(400, "Missing Details")

    return "Success: Password change complete"

@service.priv_route('/v<version>/account/alertStatus', method=['PUT','OPTIONS'])
@hooks.inrole('!admin')
@hooks.asStatusEntity()
def toggle_alert_status(account, user, groups, db, version):
    if request.method == "OPTIONS": return

    service.logger.debug("{0} GET|PUT: /account/alertStatus".format(version))

    if request.method == 'PUT' and "flagValue" in request.json:
        db.query(models.Account)\
            .filter(models.Account.accountID==account.accountID)\
            .update(request.json['flagValue'])
        try:
            db.commit()
        except:
            db.rollback()
            abort(409, "Error while updating the alert flag")



"""
Account Create
--------------
Generate Primary Account via email address
Create primary user account via email address
send activation email to primary user account
move vehicle from source account to target account and
detach existing group links.
relink device to new user account.


"""
