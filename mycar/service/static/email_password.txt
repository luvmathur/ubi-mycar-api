Password Reset Request

Please open the link below in a browser to reset your myCar password.
Reset Password: http://api.skytrace.com/v3.0/account/user/activate/@key

To cancel this request, open the link below in your browser.
Cancel Request: http://api.skytrace.com/v3.0/account/user/password/reset/@key

Sincerely, The myCar Team.

