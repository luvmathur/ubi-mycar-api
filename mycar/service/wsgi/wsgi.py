import sys, os

os.chdir(os.path.dirname(__file__))
os.chdir('..')

sys.path = [os.getcwd()] + sys.path

from account import service as account_service
from device import service as device_service
from geozone import service as geozone_service
from report import service as report_service
from static_resource import service as resource_service
from vehicle import service as vehicle_service

app = account_service
app.merge(device_service)
app.merge(geozone_service)
app.merge(report_service)
app.merge(resource_service)
app.merge(vehicle_service)

application = app

