from core import WebService, abort, get_resource, has_resource, request, response

service = WebService('resource', 'app.config')

@service.route('/v<version>/resource/js/<file>', method=['GET','OPTIONS'])
def static_js(version, file):
    """Return static resource"""
    if request.method == "OPTIONS": return

    if not has_resource(file):
        abort(404, "Not Found")

    mime_type, resource =  get_resource(file)
    base_type, sub_type = mime_type.split('/')
    if sub_type != 'javascript':
        abort(404, "Not Found")

    response.set_header('Content-Type', mime_type)
    return resource

@service.route('/v<version>/resource/image/<file>', method=['GET','OPTIONS'])
def static_image(version, file):
    """Return static resource"""
    if request.method == "OPTIONS": return

    if not has_resource(file):
        abort(404, "Not Found")

    mime_type, resource =  get_resource(file)
    base_type, sub_type = mime_type.split('/')
    if base_type != 'image':
        abort(404, "Not Found")

    response.set_header('Content-Type', mime_type)
    return resource
