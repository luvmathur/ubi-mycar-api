from core import WebService, utils, hooks, abort, request, EdmundsAPI
from stslib.database import models, errors
from hashlib import sha1
from uuid import uuid1
from time import time, mktime
from datetime import timedelta, datetime
from dateutils import relativedelta
import json

service = WebService('vehicle', 'app.config')

@service.priv_route('/v<version>/vehicle/details', ['POST','OPTIONS'])
@hooks.checksum(sha1)
def vinEdmundsLookup(db, user, groups, account, version):
    """
    Get the vehicle details from the edmunds api

    Args:
        db:
        user:
        groups:
        account:
        version:

    Returns:
        all the details in json format
    """

    if request.method == "OPTIONS": return;

    if is_valid(request.json['vin']):
        edmundsApi = EdmundsAPI(service.edmundsApiKey)
        code, details = edmundsApi.get_details(request.json['vin'])

        if 200 >= code < 300:
            service.logger.debug("Record found: {}".format(details))
        else:
            service.logger.debug("Record not-found ({})".format(code))
            return { 'error' : 'Vehicle VIN lookup failed' }

        # convert the details to JSON
        details = json.dumps(details)

        pendingInstall = db.query(models.PendingInstallation)\
        .filter(models.PendingInstallation.email == user.accountID)\
        .first()

        if not pendingInstall:
            #allow user to initiate adding additional devices
            pendingInstall = models.PendingInstallation()
            pendingInstall.accountID = "retail"
            pendingInstall.VIN = str(utils.utc_timestamp())
            pendingInstall.creationTime = str(utils.utc_timestamp())
            pendingInstall.email =  user.accountID
            pendingInstall.activationID=''
            db.add(pendingInstall)

        pendingInstall.VIN = request.json['vin']
        pendingInstall.deviceID = request.json['imei']

        try:
            db.commit()
        except errors.IntegrityError:
            db.rollback()
            abort(409, 'Failed to update pending install entry')
        except Exception as e:
            raise  e

        return { 'details':details }
    else:
        return { 'error':'Invalid VIN' }

def is_valid(vin):
    """
    Checks length and checksum to validate a given VIN

    Based on code from https://gist.github.com/krisneuharth/2176789
    and https://github.com/h3/python-libvin

    :param vin: string
    :returns: bool
    """

    POSITIONAL_WEIGHTS = [8,7,6,5,4,3,2,10,0,9,8,7,6,5,4,3,2]
    LETTER_KEY = dict(
        A=1, B=2, C=3, D=4, E=5, F=6, G=7, H=8,
        J=1, K=2, L=3, M=4, N=5,      P=7,      R=9,
             S=2, T=3, U=4, V=5, W=6, X=7, Y=8, Z=9,
    )

    vin = vin.strip().upper()

    if len(vin) != 17:
        return False

    if any(x in 'IOQ' for x in vin):
        return False

    if vin[9] in 'UZ0':
        return False

    check_digit = vin[8]

    pos = sum = 0
    for char in vin:
        value = int(LETTER_KEY[char]) if char in LETTER_KEY else int(char)
        weight = POSITIONAL_WEIGHTS[pos]
        sum += (value * weight)
        pos += 1

    calc_check_digit = int(sum) % 11

    if calc_check_digit == 10:
        calc_check_digit = 'X'

    if str(check_digit) != str(calc_check_digit):
        return False

    return True