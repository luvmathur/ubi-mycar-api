from core import WebService, utils, hooks, abort, request
from stslib.database import models, errors
from hashlib import sha1
from uuid import uuid1
from time import time, mktime
from datetime import timedelta, datetime
from dateutils import relativedelta
import math


service = WebService('device', 'app.config')

@service.priv_route('/v<version>/device/', method=['GET','PUT','OPTIONS'])
@service.priv_route('/v<version>/device/list', method=['GET','PUT','OPTIONS'])
@hooks.checksum(sha1)
def get_devices(groups, user, db, version, **kwargs):
    """Retrieve a sort list of all devices available to this user

    METHOD: GET
    ROUTE: /

    This method can be ran by any valid user. It will retrieve a shortlist
    of all devices using the users group memberships.
    """
    if request.method == "OPTIONS": return;

    service.logger.debug("{0} GET|PUT /".format(version))
    devices = db.getDevicesInGroups(groups, user.accountID)

    if request.method == 'PUT' and "preferredDeviceID" in request.json:
        user.preferredDeviceID = request.json['preferredDeviceID']
        db.commit()

    ret = []
    for device in devices:
        ret.append(utils.pack(device, [
            "deviceID",
            "vehicleID",
            "vehicleMake",
            "vehicleModel",
            "vehicleYear",
            "description",
        ]))
    
    service.logger.debug(ret)
    if not ret: return { 'devices' : 0, 'preferredDeviceID' : user.preferredDeviceID }

    return { 'devices' : ret, 'preferredDeviceID' : user.preferredDeviceID }

@service.priv_route('/v<version>/device/status/<deviceid>', method=['GET','OPTIONS'])
@hooks.checksum(sha1)
def isAvailaiblemyCarDevice(db, user, account, version, deviceid=None, **kwargs):
    """
            Check if the device is available in the UnassginedDevices table.
    Args:
        db:
        user:
        account:
        version:
        deviceid:
        **kwargs:

    Returns:
        response status
    """
    if request.method == "OPTIONS": return

    res = { 'status' : '' }

    device_details = db.query(models.UnassignedDevices)\
    .filter(models.UnassignedDevices.mobileID == deviceid)\
    .first()

    if not device_details:
        return { 'status' : 'unavailable' }
    else:
        return { 'status' : 'available' }


@service.priv_route('/v<version>/device/add', method=['POST','OPTIONS'])
@hooks.checksum(sha1)
def addDevice(db, user, groups, account, version):
    """
        Adds the device details in these tables:
            DeviceList
            DeviceUI
            Device

        Deletes the entry from PendingInstall table

    Args:
        db:
        user:
        groups:
        account:
        version:

    Returns:

    """
    if request.method == "OPTIONS": return;
    service.logger.debug("{0} POST: /device/add".format(version))
    utctsInt = utils.utc_timestamp()
    utcts = str(utctsInt)

    pendingInstall = db.query(models.PendingInstallation)\
    .filter(models.PendingInstallation.email == user.accountID)\
    .first()

    if not pendingInstall: abort(404, "No entry in pending install list")

    device = db.query(models.Device)\
    .filter(models.Device.deviceID == pendingInstall.deviceID)\
    .first()

    odometer = 0;

    if not user.preferredDeviceID:
        user.preferredDeviceID = pendingInstall.deviceID
        db.add(user)

    if not device:
        deviceList = models.DeviceList()
        deviceList.accountID = user.accountID
        deviceList.groupID = user.userID
        deviceList.deviceID = pendingInstall.deviceID
        deviceList.lastUpdateTime = utctsInt
        deviceList.creationTime = utctsInt

        deviceUiTemplate = db.query(models.DeviceUITemplate)\
        .filter(models.DeviceUITemplate.accountID == account.originAccountID)\
        .all()

        if deviceUiTemplate:
            for row in deviceUiTemplate:
                deviceUI = models.DeviceUI()
                deviceUI.deviceID = pendingInstall.deviceID
                deviceUI.setID = row.setID
                deviceUI.label = row.label
                deviceUI.type = row.type
                deviceUI.value = row.value
                deviceUI.orer = row.order
                db.add(deviceUI)

        newDevice = models.Device()
        newDevice.accountID = user.accountID
        newDevice.deviceID = pendingInstall.deviceID
        newDevice.vehicleID = pendingInstall.VIN
        newDevice.fuelCapacity = request.json['details']['fuel_capacity_l']
        newDevice.fuelEconomy = request.json['details']['economy_kpl']
        newDevice.uniqueID = pendingInstall.deviceID
        newDevice.imeiNumber = pendingInstall.deviceID
        newDevice.lastOdometerKM = odometer
        newDevice.odometerOffsetKM = 0
        newDevice.isActive = 1
        newDevice.product = "mycar"
        newDevice.description = str(request.json['details']['year']) + " " + request.json['details']['make'] + " " + request.json['details']['model']
        newDevice.vehicleMake = request.json['details']['make']
        newDevice.vehicleModel = request.json['details']['model']
        newDevice.vehicleYear = str(request.json['details']['year'])
        newDevice.installTime = utctsInt
        newDevice.Color = request.json['details']['color']
        newDevice.originalAccountID = pendingInstall.accountID
        newDevice.originalCompanyID = pendingInstall.accountID
        newDevice.originalGroupID = user.userID
        newDevice.isMarried = 1
        newDevice.deviceHistoryID = str(uuid1())
        newDevice.maintIntervalKM0 = 4828.03
        newDevice.OdometerKM0 = odometer
        newDevice.maintIntervalKM1 = 48280.32
        newDevice.OdometerKM1 = odometer
        newDevice.sold_flag=1
        newDevice.customized_flag=0

        try:
            db.add(deviceList)
            db.add(newDevice)
            db.delete(pendingInstall)
            db.commit()
        except errors.IntegrityError:
            db.rollback()
            abort(409, 'Failed to add the device.')

        return {"statusCode" : 200, "message" : "Device added successfully"}
        # res['message'] = "Device added successfully"
    else:
        if device.accountID == user.accountID:
            device.vehicleID = pendingInstall.VIN
            device.fuelCapacity = request.json['details']['fuel_capacity_l']
            device.fuelEconomy = request.json['details']['economy_kpl']
            device.description = str(request.json['details']['year']) + " " + request.json['details']['make'] + " " + request.json['details']['model']
            device.vehicleMake = request.json['details']['make']
            device.vehicleModel = request.json['details']['model']
            device.vehicleYear = str(request.json['details']['year'])
            device.installTime = utctsInt
            device.Color = request.json['details']['color']

            try:
                db.delete(pendingInstall)
                db.commit()
            except errors.IntegrityError:
                db.rollback()
                abort(409, 'Failed to add the device.')
            return {"statusCode" : 200, "message" : "Device updated successfully"}

            # res['message'] = 'Device updated successfully'
        else:
            return {"statusCode" : 405, "message" : "This device is already associated with another vehicle"}
            # abort(405,'This device is already associated with another vehicle');

@service.priv_route('/v<version>/device/location', method=['GET','OPTIONS'])
@service.priv_route('/v<version>/device/location/<deviceid>', method=['GET','OPTIONS'])
@hooks.checksum(sha1)
def get_location(db, user, groups, account, version, deviceid=None, **kwargs):
    """retrieve the last valid location details for the specified device (default: preferredDeviceID)

    METHOD: GET
    ROUTE: /location/<deviceid>, /location

    This method can be ran by any valid user. It will retrieve the last
    valid location information for the vehicle
    """

    """
    SELECT  `timestamp`
    ,       address
    ,       latitude
    ,       longitude 
    FROM    EventData 
    WHERE   DeviceID = '354898040623307' 
    AND     accountID = 'ian@skytrace.com' 
    and     HDOP < 5 
    and     latitude != 0 
    and     longitude != 0 
    order by `timestamp` 
    LIMIT 1;
    """
    if request.method == "OPTIONS": return

    service.logger.debug("{0} GET: /location, /details/<deviceid>".format(version))
    _deviceid = deviceid if deviceid else user.preferredDeviceID
    service.logger.debug("DEVICEID: {0}".format(_deviceid))

    eventData = db.query(models.EventData)\
        .filter(models.EventData.accountID == user.accountID)\
        .filter(models.EventData.deviceID == _deviceid)\
        .filter(models.EventData.HDOP < 5)\
        .filter(models.EventData.latitude != 0)\
        .filter(models.EventData.longitude != 0)\
        .order_by(models.EventData.timestamp.desc())\
        .first()

    if not eventData: return { "statusCode" : 404, "message" : "Device not found"}

    if 'update' in request.GET:
        user.preferredDeviceID = _deviceid
        db.commit()

    lastEventDateTime = utils.timestamp2tztime(eventData.timestamp, user.timeZone)
    val = {
        'lastEventDate' : lastEventDateTime.strftime('%A, %B %d, %y'),
        'lastEventTime' : lastEventDateTime.strftime('%I:%M:%S %p'),
        'lastValidLocation' : {
            'address' : eventData.address if eventData.address else 'Unknown',
            'lat' : float(eventData.latitude),
            'lng' : float(eventData.longitude),
        },
    }

    service.logger.debug(val)
    return val


def generate_agreement_time_remaining_description(remaining_days):
    # Calculate months remaining
    today = datetime.now()
    expires = today + relativedelta(days=+remaining_days)
    delta = relativedelta(expires, today)
    months_remaining = delta.years * 12 + delta.months
    if months_remaining == 0:
        return "{0} Days".format(delta.days)
    else:
        return "{0} Months".format(months_remaining)


def generate_agreement_record(unit_converter, agreement):
    if agreement is None:
        return {}
    else:
        return {
            'maxDistance': unit_converter.distance2local(agreement.agreementKM),
            'remainingDistance': unit_converter.distance2local(agreement.remainingKM),
            'distancePercent': agreement.remainingKMPercent,
            'maxDays': agreement.agreementDays,
            'daysPercent': agreement.remainingDaysPercent,
            'remainingDays': agreement.remainingDays,
            'remainingDuration': generate_agreement_time_remaining_description(agreement.remainingDays),
            'type': agreement.agreementType.Name,
            'desc': agreement.Description
        }


def find_oldest_valid_agreement(agreements):
    # look through agreements for the oldest, non-expired record
    oldest_valid_agreement = None
    for agreement in agreements:
        if (agreement.remainingDays > 0 and
           (oldest_valid_agreement is None or agreement.StartDate < oldest_valid_agreement.StartDate)):
            oldest_valid_agreement = agreement

    return oldest_valid_agreement


def generate_agreement_details(version, unit_converter, agreements):
    if version == "3.0":
        agreement = find_oldest_valid_agreement(agreements)
        return generate_agreement_record(unit_converter, agreement)
    else:
        agreement_detail_list = []
        for agreement in agreements:
            agreement_detail_list.append(generate_agreement_record(unit_converter, agreement))
        return agreement_detail_list


@service.priv_route('/v<version>/device/detail', method=['GET','OPTIONS'])
@service.priv_route('/v<version>/device/detail/<deviceid>', method=['GET','OPTIONS'])
@hooks.checksum(sha1)
def get_device(db, user, groups, account, version, deviceid=None, **kwargs):
    """Get details about a specific device (default: preferredDeviceID)

    METHOD: GET
    ROUTE: /details, /details/<deviceid>

    This method can be ran by any valid user. The specified device will be tested
    for membership by the user and optionally set the users preferred device id
    """
    if request.method == "OPTIONS": return

    service.logger.debug("{0} GET: /details, /details/<deviceid>".format(version))
    _deviceid = deviceid if deviceid else user.preferredDeviceID

    device = db.getDevicesInGroups(groups, user.accountID)\
        .filter(models.Device.deviceID == _deviceid).first()

    if not device: abort(404, "Device Not Found")

    dealer = db.query(models.AdpDealerMap)\
        .filter(models.AdpDealerMap.accountID == device.originalAccountID)\
        .filter(models.AdpDealerMap.companyID == device.originalCompanyID)\
        .first()

    if 'update' in request.GET and deviceid:
        service.logger.debug('Got a UPDATE request to set preferred device id to %s' % _deviceid)
        user.preferredDeviceID = _deviceid
        db.commit()

    theft = db.query(models.DevicePublic)\
        .filter(models.DevicePublic.deviceID == device.deviceID)\
        .filter(models.DevicePublic.type == 'THEFT')\
        .first()

    convert = utils.UnitConverter(account)
    maint_detail = device.nextServiceDetails

    diags = []
    if device.lastFaultCode:
        _fault_codes = device.lastFaultCode.split('=')
        if len(_fault_codes) > 2:
            for _fault_code in _fault_codes[3].split(','):
                fault = db.query(models.obdiicodes)\
                    .filter(models.obdiicodes.code == _fault_code)\
                    .order_by(models.obdiicodes.priority)\
                    .first()
                if fault: diags.append({'code' : fault.code, 'message' : fault.message, 'description' : fault.description, })
                else: diags.append({'code' : _fault_code, 'message' : 'Unknown', 'description' : 'Code unknown, contact your manufacturer', })


    _services = []
    for ui_element in db.query(models.DeviceUI).filter(models.DeviceUI.deviceID==_deviceid).order_by(models.DeviceUI.order):
        _services.append({
            'set' : ui_element.setID,
            'type' : ui_element.type,
            'label' : ui_element.label,
            'value' : ui_element.value,
        })

    if not device.lastGPSTimestamp or device.lastGPSTimestamp==0:
        lastGPSTimestamp = datetime.fromtimestamp(0)
    else:
        lastGPSTimestamp = utils.timestamp2tztime(device.lastGPSTimestamp, user.timeZone)

    val = {
        'deviceID' : device.deviceID,
        'vehicleID' : device.vehicleID,
        'vehicleMake' : device.vehicleMake,
        'vehicleModel' : device.vehicleModel,
        'vehicleYear' : device.vehicleYear,
        'description' : device.description,
        'odometer' : convert.distance2local(device.odometerKM),
        'diags' : diags,
        'theft' : True if theft else False,
        'maintenance' : {},
        'fuel' : {
            'capacity' : float(device.fuelCapacity),
            'percent' : float(device.lastFuelPercent),
            'volume' : convert.volume2local(float(device.fuelCapacity) * float(device.lastFuelPercent)),
        },
        'agreement' : generate_agreement_details(version, convert, device.agreements),
        'location' : {
            'lastGPSDate' : lastGPSTimestamp.strftime('%A, %B %d, %y'),
            'lastGPStime' : lastGPSTimestamp.strftime('%I:%M:%S %p'),
            'address' : device.lastValidAddress if device.lastValidAddress else 'Unknown',
            'lat' : float(device.lastValidLatitude) if device.lastValidLatitude else 0,
            'lng' : float(device.lastValidLongitude) if device.lastValidLongitude else 0,
        },
        'services' : _services,
    }

    if dealer:
        val['maintenance']={
            'interval_index': maint_detail['index'],
            'interval': round(convert.distance2local(maint_detail['intervalKM']), 0),
            'remaining': convert.distance2local(maint_detail['remainingKM']),
            'serviceOdometer': convert.distance2local(maint_detail['serviceOdometerKM']),
            'percent': maint_detail['percent'],
            'dealerName': dealer.dealerName,
            'dealerAddress': dealer.dealerAddress,
            'dealerCity': dealer.dealerCity,
            'dealerState': dealer.dealerState,
            'dealerLat': dealer.dealerLat,
            'dealerLng': dealer.dealerLng,
        }
    else:
        val['maintenance'] = {
            'interval_index': maint_detail['index'],
            'interval': round(convert.distance2local(maint_detail['intervalKM']), 0),
            'remaining': convert.distance2local(maint_detail['remainingKM']),
            'serviceOdometer': convert.distance2local(maint_detail['serviceOdometerKM']),
            'percent': maint_detail['percent'],
            'dealerName': "",
            'dealerAddress': "",
            'dealerCity': "",
            'dealerState': "",
            'dealerLat': "",
            'dealerLng': "",
        }

    service.logger.debug(val)
    return val
    

@service.priv_route('/v<version>/device/reset', method=['POST','OPTIONS'])
@service.priv_route('/v<version>/device/reset/<deviceid>', method=['POST','OPTIONS'])
@hooks.asStatusEntity()
def get_reset(db, user, groups, account, version, deviceid=None, **kwargs):
    """Reset the maintenance for this vehicle.

    METHOD: GET
    ROUTE: /reset/<deviceid>

    This method can be ran by any valid user. It will reset the
    maintenance odometers to the current odometer value
    """
    if request.method == "OPTIONS": return

    service.logger.debug("{0} POST: /reset/{1}".format(version, deviceid))
    _deviceid = deviceid if deviceid else user.preferredDeviceID
    device = db.getDevicesInGroups(groups, user.accountID)\
        .filter(models.Device.deviceID == _deviceid).first()

    if not device: abort(404, "Device Not Found")

    if 'update' in request.GET:
        user.preferredDeviceID = _deviceid

    index = request.json.get('index', -1)
    if index == 0:
        device.maintOdometerKM0 = device.lastOdometerKM
        db.commit()
    elif index == 1:
        device.maintOdometerKM1 = device.lastOdometerKM
        db.commit()
    else:
        abort(500, "Maintenance Odometer index not valid.")

    return "SUCCESS: Service Odometer Reset"

@service.priv_route('/v<version>/device/share', method=['POST','OPTIONS'])
@service.priv_route('/v<version>/device/share/<deviceid>', method=['POST','OPTIONS'])
@hooks.asStatusEntity()
def get_share(db, user, groups, version, deviceid=None, **kw):
    """Share device with another person

    POST: /share/<deviceid>

    This function can be ran by any valid user. it will create a link
    in the DevicePublic table that will allow other, non authenticated users
    to access there device telemetry. This is used for social and theft recovery
    """
    if request.method == "OPTIONS": return

    service.logger.debug("{0} POST: /share/{1}".format(version, deviceid))

    _deviceid = deviceid if deviceid else user.preferredDeviceID
    device = db.getDevicesInGroups(groups, user.accountID)\
        .filter(models.Device.deviceID == _deviceid).first()

    if not device: abort(404, "Device Not Found")

    _type = request.json.get('type', 'SOCIAL')
    if _type == 'THEFT':
        current = db.query(models.DevicePublic)\
            .filter(models.DevicePublic.deviceID == _deviceid)\
            .filter(models.DevicePublic.type == 'THEFT')\
            .first()

        if request.json.get('action') == 'DELETE' and current:
            try:
                db.delete(current)
                db.commit()
                return "SUCCESS: Removed shared telemetry."
            except:
                db.rollback()
                abort(500, "Unable to revoke shared vehicle telemetry")

        elif current:
            ## It was already shared. just go on.
            return "SUCCESS: Shared telemetry."

    guid = uuid1().hex
    default_limit = int(mktime((datetime.now() + timedelta(days=5)).timetuple()))

    public_access = models.DevicePublic()
    public_access.deviceID = _deviceid
    public_access.uuid = guid
    public_access.type = _type
    public_access.validTimestamp = request.json.get('expires', default_limit)
    public_access.createTimestamp = utils.utc_timestamp()
    db.add(public_access)
    
    try:
        db.commit()
        service.send_share_device(request.json.get('email', user.notifyEmail), guid)
    except:
        db.rollback()
        abort(500, "Unable to share vehicle telemetry")
    
    return "SUCCESS: Shared telemetry."


@service.priv_route('/v<version>/device/threshold', method=['GET', 'OPTIONS'])
@service.db
def get_speed_device(db, account, version, **kwargs):
    if request.method == "OPTIONS":
        return

    service.logger.debug("{0} GET: /device/threshold".format(version))
    device = db.query(models.Device) \
        .filter(models.Device.accountID == account.accountID) \
        .first()
    if device.speedLimitKPH is None:
        speed = 0
    else:
        speed = round(float(device.speedLimitKPH) * 0.621371, 2)
    return {'speed': speed}


@service.priv_route('/v<version>/device/threshold', method=['POST', 'OPTIONS'])
@service.db
def set_speed_device(db, account, version, **kwargs):
    if request.method == "OPTIONS":
        return

    service.logger.debug("{0} POST: /device/threshold".format(version))
    device = db.query(models.Device) \
        .filter(models.Device.accountID == account.accountID) \
        .first()
    speed = request.json['speed']
    device.speedLimitKPH = float(speed) * 1.60934
    service.logger.debug(device.speedLimitKPH)
    try:
        db.commit()
    except:
        db.rollback()
        abort(500, "Unable to save the overspeed threshold limit")
