## Running and Testing

To run this software locally you can use the docker build script. This will take
care of all the dependencies and requirements to run the api.

### Build Docker Container
The following command will build a new docker container.

__At the root of the project__
```bash
# Build the container
docker build -t api .

# Verify the container
docker images
```

### Running a docker container
Next, we can run the new container. We have a great many options here but these 
are the basics.

__NOTE__: You might need to change the app.config to point to the database

```bash
# Run the docker container
sudo docker run -t -i -p 80:5000 -v $(pwd)/service:/srv/api api

# OR you can run the container with a shell with more debugging options
sudo docker run -t -i -p 80:5000 -v $(pwd)/service:/srv/api api /bin/bash
# after the container starts
cd /srv/api/wsgi
gunicorn -b 0.0.0.0:5000 wsgi
```

Explaination of arguments
* -t Execute with a TTY
* -i Execute in interactive mode
* -p Bind host port to container port ( ie: HOST:CONTAINER)
* -v Mount local directory into container direcotry

That last option is important. You can edit the files on your host and effect
the files in the container for development.
