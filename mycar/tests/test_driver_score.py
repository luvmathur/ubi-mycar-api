import unittest

from service.report_impl import driver_score

class TestDriverScore(unittest.TestCase):
    FIFTEEN_MINUTES = 60*15

    def test_0(self):
        self.assertEqual(driver_score(self.FIFTEEN_MINUTES, 0), 1.0)

    def test_1(self):
        self.assertEqual(driver_score(self.FIFTEEN_MINUTES, 1), 1.0)

    def test_2(self):
        self.assertAlmostEqual(driver_score(self.FIFTEEN_MINUTES, 2), 1.0 - (1.0/15))

    def test_15(self):
        self.assertAlmostEqual(driver_score(self.FIFTEEN_MINUTES, 15), 0.0 + (1.0/15))

    def test_16(self):
        self.assertEqual(driver_score(self.FIFTEEN_MINUTES, 16), 0.0)

    def test_range(self):
        for i in range(2, 16):
            self.assertAlmostEqual(driver_score(self.FIFTEEN_MINUTES, i), 1.0 - (i-1)/15.0)
